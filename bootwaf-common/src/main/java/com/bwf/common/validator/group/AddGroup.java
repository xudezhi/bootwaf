/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.common.validator.group;

/**
 * 新增数据 Group
 *
 * @author XDZ 1136277749@qq.com
 */
public interface AddGroup {
}
