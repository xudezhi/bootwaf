/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.common.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 *
 * @author XDZ 1136277749@qq.com
 */
public class R extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	
	public enum RCode {
		//成功
		SUCCESS(0, "成功"),		
		//没有权限
		NO_POWER(301,"没有权限"), 
		//未知错误
		UNKNOWN_ERROR(500, "未知异常，请联系管理员"),
		//未登录
		UN_LOGIN(401,"未登录");					
		//返回码
		int code;
		//返回信息
		String msg;

		private RCode(int code, String msg) {
			this.code = code;
			this.msg = msg;
		}

		public Integer getCode() {
			return code;
		}
		
		public String getMsg() {
			return msg;
		}
	}
	
	public R() {
		put("code", RCode.SUCCESS.code);
		put("msg", RCode.SUCCESS.msg);
	}
	
	public static R error() {
		return error(RCode.UNKNOWN_ERROR, RCode.UNKNOWN_ERROR.msg);
	}
	
	public static R error(String msg) {
		return error(RCode.UNKNOWN_ERROR, msg);
	}
	
	public static R error(RCode rcode, String msg) {
		R r = new R();
		r.put("code", rcode.code);
		r.put("msg", msg);
		return r;
	}

	public static R ok(String msg) {
		R r = new R();
		r.put("msg", msg);
		return r;
	}
	
	public static R ok(Map<String, Object> map) {
		R r = new R();
		r.putAll(map);
		return r;
	}
	
	public static R ok() {
		return new R();
	}

	@Override
	public R put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
