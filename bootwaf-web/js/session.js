var token = sessionStorage.getItem("token");
if(token==null){
	location = '/html/login.html';
}
//jquery全局配置
$.ajaxSetup({
    dataType: "json",
    cache: false,
    crossDomain: true,  
    headers: {
        "token": token
    },
    complete:function(xhr){
    	if (xhr.responseJSON && "code" in xhr.responseJSON && xhr.responseJSON.code === 401) {
    		sessionStorage.removeItem("token");
            location = '/html/login.html';
        }else if(xhr.responseJSON && "code" in xhr.responseJSON && xhr.responseJSON.code != 0){
			layer.alert(xhr.responseJSON.msg);
        }
    	
    },
    error:function(){
    	layer.alert("网络异常！");
    }
});

