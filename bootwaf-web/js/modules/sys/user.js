
Vue.config.productionTip = false;
Vue.config.devtools = true;
var vm = new Vue({
  el: "#root-app",
  data:{
        q:{
            username: null
        },
        showList: true,
        title:"用户管理",
        roleList:{},
        user:{
            status:1,
            deptId:null,
            deptName:null,
            roleIdList:[]
        },
        setting: {
        	    data: {
        	        simpleData: {
        	            enable: true,
        	            idKey: "deptId",
        	            pIdKey: "parentId",
        	            rootPId: -1
        	        },
        	        key: {
        	            url:"nourl"
        	        }
        	    }
        	},
        ztree:{}
    }, 
    mounted: function mounted() {
  	  $("#jqGrid").jqGrid({
	        url: baseURL+"/sys/user/list",
	        datatype: "json",
	        colModel: [			
				{ label: '用户ID', name: 'userId', index: "user_id", width: 45, key: true },
				{ label: '用户名', name: 'username', width: 75 },
	            { label: '所属部门', name: 'deptName', sortable: false, width: 75 },
				{ label: '邮箱', name: 'email', width: 90 },
				{ label: '手机号', name: 'mobile', width: 100 },
				{ label: '状态', name: 'status', width: 60, formatter: function(value, options, row){
					return value === 0 ? 
						'<span class="label label-danger">禁用</span>' : 
						'<span class="label label-success">正常</span>';
				}},
				{ label: '创建时间', name: 'createTime', index: "create_time", width: 85,formatter:function(value,options,row){
					var d = new Date(value);
			        var curr_date = d.getDate();
			        var curr_month = d.getMonth() + 1; 
			        var curr_year = d.getFullYear();
			        var curr_minutes=d.getMinutes();
			        String(curr_month).length < 2 ? (curr_month = "0" + curr_month): curr_month;
			        String(curr_date).length < 2 ? (curr_date = "0" + curr_date): curr_date;
			        String(curr_minutes).length < 2 ? (curr_minutes = "0" + curr_minutes): curr_minutes;
			        var yyyyMMdd = curr_year + "-" + curr_month +"-"+ curr_date+" "+d.getHours()+":"+curr_minutes;
			        return yyyyMMdd;
				}}
	        ],
			viewrecords: true,
	        height: 500,
	        rowNum: 30,
			rowList : [10,30,50],
	        rownumbers: true, 
	        rownumWidth: 25, 
	        autowidth:true,
	        multiselect: true,
	        pager: "#jqGridPager",
	        jsonReader : {
	            root: "page.list",
	            page: "page.currPage",
	            total: "page.totalPage",
	            records: "page.totalCount"
	        },
	        prmNames : {
	            page:"page", 
	            rows:"limit", 
	            order: "order"
	        },
	        gridComplete:function(){
	        	//隐藏grid底部滚动条
	        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
	        }
	    });
    },
    methods: {
    	//判断是否有权限
    	hasPermission:function(value){
    		var permitted=false;
    		 $.ajax({
                 type: "POST",
                 async: false,
                 url: baseURL+"/sys/user/permission/isPermitted?permission="+value+"&t="+$.now(),
                 contentType: "application/json",
                 success: function(r){
                	 if(r.code==0){
                		 permitted=r.isPermitted;
         			}else{
         				permitted=false;
         			}
                 }
             });
    		return permitted;
    	},
        query: function () {
            this.reload();
        },
        add: function(){
            this.showList = false;
            this.title = "新增";
            this.roleList = {};
            this.user = {deptName:null, deptId:null, status:1, roleIdList:[]};

            //获取角色信息
            this.getRoleList();

            this.getDept();
        },
        getDept: function(){
        	var that=this;
            //加载部门树
            $.get(baseURL+"/sys/dept/list", function(r){
                that.ztree = $.fn.zTree.init($("#deptTree"), that.setting, r);
                var node = that.ztree.getNodeByParam("deptId", that.user.deptId);
                if(node != null){
                    that.ztree.selectNode(node);

                    that.user.deptName = node.name;
                }
            })
        },
        update: function () {
            var userId = getSelectedRow();
            if(userId == null){
                return ;
            }

            this.showList = false;
            this.title = "修改";

            this.getUser(userId);
            //获取角色信息
            this.getRoleList();
        },
        permissions: function () {
            var userId = getSelectedRow();
            if(userId == null){
                return ;
            }

            window.location.href=baseURL+"/sys/permissions/index/"+userId;
        },
        del: function () {
            var userIds = getSelectedRows();
            if(userIds == null){
                return ;
            }
            var that=this;
            confirm('确定要删除选中的记录？', function(){
                $.ajax({
                    type: "POST",
                    url: baseURL+"/sys/user/delete",
                    contentType: "application/json",
                    data: JSON.stringify(userIds),
                    success: function(r){
                        if(r.code == 0){
                            alert('操作成功', function(){
                                that.reload();
                            });
                        }else{
                            alert(r.msg);
                        }
                    }
                });
            });
        },
        saveOrUpdate: function () {
            var url = this.user.userId == null ? baseURL+"/sys/user/save" : baseURL+"/sys/user/update";
            var that=this;
            $.ajax({
                type: "POST",
                url: url,
                contentType: "application/json",
                data: JSON.stringify(that.user),
                success: function(r){
                    if(r.code === 0){
                        alert('操作成功', function(){
                            that.reload();
                        });
                    }else{
                        alert(r.msg);
                    }
                }
            });
        },
        getUser: function(userId){
        	var that=this;
            $.get(baseURL+"/sys/user/info/"+userId, function(r){
                that.user = r.user;
                that.user.password = null;

                that.getDept();
            });
        },
        getRoleList: function(){
        	var that=this;
            $.get(baseURL+"/sys/role/select", function(r){
                that.roleList = r.list;
            });
        },
        deptTree: function(){
        	var that=this;
            layer.open({
                type: 1,
                offset: '50px',
                skin: 'layui-layer-molv',
                title: "选择部门",
                area: ['300px', '450px'],
                shade: 0,
                shadeClose: false,
                content: jQuery("#deptLayer"),
                btn: ['确定', '取消'],
                btn1: function (index) {
                    var node = that.ztree.getSelectedNodes();
                    //选择上级部门
                    that.user.deptId = node[0].deptId;
                    that.user.deptName = node[0].name;

                    layer.close(index);
                }
            });
        },
        reload: function () {
        	var that=this;
            this.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam','page');
            $("#jqGrid").jqGrid('setGridParam',{
                postData:{'username': that.q.username},
                page:page
            }).trigger("reloadGrid");
        }
    }
});



