$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + "/tb/test/list",
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true },
			{ label: '所在类', name: 'clazz', index: 'clazz', width: 80 }, 			
			{ label: '类说明', name: 'clazzExplain', index: 'clazz_explain', width: 80 }, 			
			{ label: '方法名', name: 'method', index: 'method', width: 80 }, 			
			{ label: '方法说明', name: 'methodExplain', index: 'method_explain', width: 80 }, 			
			{ label: '请求方式', name: 'requestMethod', index: 'request_method', width: 80 }, 			
			{ label: '响应方式', name: 'resType', index: 'res_type', width: 80 }, 			
			{ label: '请求路径', name: 'pathUrl', index: 'path_url', width: 80 }, 			
			{ label: '限制用户', name: 'forUsers', index: 'for_users', width: 80 }, 			
			{ label: '限制角色', name: 'forRoles', index: 'for_roles', width: 80 }, 			
			{ label: '所需权限', name: 'permissions', index: 'permissions', width: 80 }, 			
			{ label: '开发者', name: 'author', index: 'author', width: 80 }, 			
			{ label: '创建者', name: 'createBy', index: 'create_by', width: 80 }, 			
			{ label: '创建时间', name: 'createDate', index: 'create_date', width: 80 }, 			
			{ label: '更新者', name: 'updateBy', index: 'update_by', width: 80 }, 			
			{ label: '更新时间', name: 'updateDate', index: 'update_date', width: 80 }, 			
			{ label: '备注信息', name: 'remarks', index: 'remarks', width: 80 }, 			
			{ label: '删除标记', name: 'delFlag', index: 'del_flag', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#root-app',
	data:{
		showList: true,
		title: null,
		test: {}
	},
	methods: {
	    //判断是否有权限
    	hasPermission:function(value){
    		var permitted=false;
    		 $.ajax({
                 type: "POST",
                 async: false,
                 url: baseURL+"/sys/user/permission/isPermitted?permission="+value+"&t="+$.now(),
                 contentType: "application/json",
                 success: function(r){
                	 if(r.code==0){
                		 permitted=r.isPermitted;
         			}else{
         				permitted=false;
         			}
                 }
             });
    		return permitted;
    	},
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.test = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.test.id == null ? "/tb/test/save" : "/tb/test/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.test),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "/tb/test/delete",
                        contentType: "application/json",
                        data: JSON.stringify(ids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(id){
			$.get(baseURL + "/tb/test/info/"+id, function(r){
                vm.test = r.test;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});