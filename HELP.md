# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/maven-plugin/reference/html/#build-image)

#启动说明
1.执行数据库 /resource/db/bootwaf.sql 
2.修改数据库连接配置信息
3.启动bootwaf-server作为后台项目
4.配置nginx ,参考/resource/nginx-bootwaf.conf
5.访问nginx地址

#测试账号
测试账号：admin admin

#项目说明
1.bootwaf-common 封装一些底层同用的工具类等
2.bootwaf-dynamic-datasource 用于配置多数据源，如果使用需要修改配置文件，增加数据源，在service上增加数据源注解，同时需要将这个项目作为jar包导入到bootwaf-server中
3.bootwaf-generator 作为代码生成工具，使用时需要配置数据库地址，同时需要设置生成的属性类，可以自己调整使用
4.bootwaf-server 作为后台项目启动，提供接口供前台调用。
5.bootwaf-web 作为纯前端的项目，控制页面跳转和调用接口呈现数据，采用vue+layer,未使用element-ui，可以根据需要调整，同时可以根据需要修改代码生成工具