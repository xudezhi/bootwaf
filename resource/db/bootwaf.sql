/*
 Navicat Premium Data Transfer

 Source Server         : localhost5.7
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : bootwaf

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 09/06/2020 17:34:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('bootwafScheduler', 'TASK_1', 'DEFAULT', '0 0/30 * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TRIG_INST_NAME`(`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY`(`SCHED_NAME`, `INSTANCE_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_FT_J_G`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_T_G`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TG`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_J_REQ_RECOVERY`(`SCHED_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_J_GRP`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('bootwafScheduler', 'TASK_1', 'DEFAULT', NULL, 'com.bwf.modules.job.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002C636F6D2E6277662E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001725EF7C6607874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740007626F6F7477616674000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('bootwafScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('bootwafScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('bootwafScheduler', 'CASIC-PC1591694649947', 1591695255682, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_J`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_C`(`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_T_G`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_STATE`(`SCHED_NAME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_STATE`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_G_STATE`(`SCHED_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NEXT_FIRE_TIME`(`SCHED_NAME`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST`(`SCHED_NAME`, `TRIGGER_STATE`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('bootwafScheduler', 'TASK_1', 'DEFAULT', 'TASK_1', 'DEFAULT', NULL, 1591696800000, 1591695000000, 5, 'WAITING', 'CRON', 1590731376000, 0, NULL, 2, 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002C636F6D2E6277662E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001725EF7C6607874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740007626F6F7477616674000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES (1, 'testTask', 'bootwaf', '0 0/30 * * * ?', 0, '参数测试', '2020-05-29 13:46:36');

-- ----------------------------
-- Table structure for schedule_job_log
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log`  (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `job_id`(`job_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 158 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------
INSERT INTO `schedule_job_log` VALUES (1, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-05-29 14:00:00');
INSERT INTO `schedule_job_log` VALUES (2, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-05-29 14:30:00');
INSERT INTO `schedule_job_log` VALUES (3, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-05-29 15:00:00');
INSERT INTO `schedule_job_log` VALUES (4, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 10:00:00');
INSERT INTO `schedule_job_log` VALUES (5, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 10:30:00');
INSERT INTO `schedule_job_log` VALUES (6, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-02 11:00:00');
INSERT INTO `schedule_job_log` VALUES (7, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 11:30:00');
INSERT INTO `schedule_job_log` VALUES (8, 1, 'testTask', 'bootwaf', 0, NULL, 3, '2020-06-02 12:00:01');
INSERT INTO `schedule_job_log` VALUES (9, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 12:30:00');
INSERT INTO `schedule_job_log` VALUES (10, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-02 13:00:00');
INSERT INTO `schedule_job_log` VALUES (11, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-02 13:30:00');
INSERT INTO `schedule_job_log` VALUES (12, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 14:00:00');
INSERT INTO `schedule_job_log` VALUES (13, 1, 'testTask', 'bootwaf', 0, NULL, 11, '2020-06-02 14:30:00');
INSERT INTO `schedule_job_log` VALUES (14, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-02 15:00:00');
INSERT INTO `schedule_job_log` VALUES (15, 1, 'testTask', 'bootwaf', 0, NULL, 14, '2020-06-02 15:30:00');
INSERT INTO `schedule_job_log` VALUES (16, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 16:00:00');
INSERT INTO `schedule_job_log` VALUES (17, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-02 16:30:00');
INSERT INTO `schedule_job_log` VALUES (18, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 17:00:00');
INSERT INTO `schedule_job_log` VALUES (19, 1, 'testTask', 'bootwaf', 0, NULL, 18, '2020-06-02 17:30:00');
INSERT INTO `schedule_job_log` VALUES (20, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 18:00:00');
INSERT INTO `schedule_job_log` VALUES (21, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-02 18:30:00');
INSERT INTO `schedule_job_log` VALUES (22, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 19:00:00');
INSERT INTO `schedule_job_log` VALUES (23, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 19:30:00');
INSERT INTO `schedule_job_log` VALUES (24, 1, 'testTask', 'bootwaf', 0, NULL, 6, '2020-06-02 20:00:00');
INSERT INTO `schedule_job_log` VALUES (25, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 20:30:00');
INSERT INTO `schedule_job_log` VALUES (26, 1, 'testTask', 'bootwaf', 0, NULL, 12, '2020-06-02 21:00:00');
INSERT INTO `schedule_job_log` VALUES (27, 1, 'testTask', 'bootwaf', 0, NULL, 17, '2020-06-02 21:30:00');
INSERT INTO `schedule_job_log` VALUES (28, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 22:00:00');
INSERT INTO `schedule_job_log` VALUES (29, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 22:30:00');
INSERT INTO `schedule_job_log` VALUES (30, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-02 23:00:00');
INSERT INTO `schedule_job_log` VALUES (31, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-02 23:30:00');
INSERT INTO `schedule_job_log` VALUES (32, 1, 'testTask', 'bootwaf', 0, NULL, 762, '2020-06-03 00:00:00');
INSERT INTO `schedule_job_log` VALUES (33, 1, 'testTask', 'bootwaf', 0, NULL, 30, '2020-06-03 00:30:00');
INSERT INTO `schedule_job_log` VALUES (34, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 01:00:00');
INSERT INTO `schedule_job_log` VALUES (35, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 01:30:00');
INSERT INTO `schedule_job_log` VALUES (36, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 02:00:00');
INSERT INTO `schedule_job_log` VALUES (37, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 02:30:00');
INSERT INTO `schedule_job_log` VALUES (38, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 03:00:00');
INSERT INTO `schedule_job_log` VALUES (39, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 03:30:00');
INSERT INTO `schedule_job_log` VALUES (40, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 04:00:00');
INSERT INTO `schedule_job_log` VALUES (41, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 04:30:00');
INSERT INTO `schedule_job_log` VALUES (42, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 05:00:00');
INSERT INTO `schedule_job_log` VALUES (43, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 05:30:00');
INSERT INTO `schedule_job_log` VALUES (44, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 06:00:00');
INSERT INTO `schedule_job_log` VALUES (45, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 06:30:00');
INSERT INTO `schedule_job_log` VALUES (46, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 07:00:00');
INSERT INTO `schedule_job_log` VALUES (47, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 07:30:00');
INSERT INTO `schedule_job_log` VALUES (48, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 08:00:00');
INSERT INTO `schedule_job_log` VALUES (49, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 08:30:00');
INSERT INTO `schedule_job_log` VALUES (50, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 09:00:00');
INSERT INTO `schedule_job_log` VALUES (51, 1, 'testTask', 'bootwaf', 0, NULL, 3, '2020-06-03 09:30:00');
INSERT INTO `schedule_job_log` VALUES (52, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 10:00:00');
INSERT INTO `schedule_job_log` VALUES (53, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 10:30:00');
INSERT INTO `schedule_job_log` VALUES (54, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 11:00:00');
INSERT INTO `schedule_job_log` VALUES (55, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 11:30:00');
INSERT INTO `schedule_job_log` VALUES (56, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 12:00:00');
INSERT INTO `schedule_job_log` VALUES (57, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 12:30:00');
INSERT INTO `schedule_job_log` VALUES (58, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 13:00:00');
INSERT INTO `schedule_job_log` VALUES (59, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 13:30:00');
INSERT INTO `schedule_job_log` VALUES (60, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 14:00:00');
INSERT INTO `schedule_job_log` VALUES (61, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 14:30:00');
INSERT INTO `schedule_job_log` VALUES (62, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 15:00:00');
INSERT INTO `schedule_job_log` VALUES (63, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 15:30:00');
INSERT INTO `schedule_job_log` VALUES (64, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 16:00:00');
INSERT INTO `schedule_job_log` VALUES (65, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 16:30:00');
INSERT INTO `schedule_job_log` VALUES (66, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 17:00:00');
INSERT INTO `schedule_job_log` VALUES (67, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 17:30:00');
INSERT INTO `schedule_job_log` VALUES (68, 1, 'testTask', 'bootwaf', 0, NULL, 3, '2020-06-03 18:00:00');
INSERT INTO `schedule_job_log` VALUES (69, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-03 18:30:00');
INSERT INTO `schedule_job_log` VALUES (70, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 19:00:00');
INSERT INTO `schedule_job_log` VALUES (71, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 19:30:00');
INSERT INTO `schedule_job_log` VALUES (72, 1, 'testTask', 'bootwaf', 0, NULL, 66, '2020-06-03 20:00:00');
INSERT INTO `schedule_job_log` VALUES (73, 1, 'testTask', 'bootwaf', 0, NULL, 59, '2020-06-03 20:30:00');
INSERT INTO `schedule_job_log` VALUES (74, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 21:00:00');
INSERT INTO `schedule_job_log` VALUES (75, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 21:30:00');
INSERT INTO `schedule_job_log` VALUES (76, 1, 'testTask', 'bootwaf', 0, NULL, 40, '2020-06-03 22:00:00');
INSERT INTO `schedule_job_log` VALUES (77, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 22:30:00');
INSERT INTO `schedule_job_log` VALUES (78, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-03 23:00:00');
INSERT INTO `schedule_job_log` VALUES (79, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-03 23:30:00');
INSERT INTO `schedule_job_log` VALUES (80, 1, 'testTask', 'bootwaf', 0, NULL, 377, '2020-06-04 00:00:00');
INSERT INTO `schedule_job_log` VALUES (81, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-04 00:30:00');
INSERT INTO `schedule_job_log` VALUES (82, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-04 01:00:00');
INSERT INTO `schedule_job_log` VALUES (83, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-04 01:30:00');
INSERT INTO `schedule_job_log` VALUES (84, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-04 02:00:00');
INSERT INTO `schedule_job_log` VALUES (85, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 02:30:00');
INSERT INTO `schedule_job_log` VALUES (86, 1, 'testTask', 'bootwaf', 0, NULL, 7, '2020-06-04 03:00:00');
INSERT INTO `schedule_job_log` VALUES (87, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 03:30:00');
INSERT INTO `schedule_job_log` VALUES (88, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 04:00:00');
INSERT INTO `schedule_job_log` VALUES (89, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 04:30:00');
INSERT INTO `schedule_job_log` VALUES (90, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-04 05:00:00');
INSERT INTO `schedule_job_log` VALUES (91, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 05:30:00');
INSERT INTO `schedule_job_log` VALUES (92, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 06:00:00');
INSERT INTO `schedule_job_log` VALUES (93, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 06:30:00');
INSERT INTO `schedule_job_log` VALUES (94, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-04 07:00:00');
INSERT INTO `schedule_job_log` VALUES (95, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-04 07:30:00');
INSERT INTO `schedule_job_log` VALUES (96, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 08:00:00');
INSERT INTO `schedule_job_log` VALUES (97, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-04 08:30:00');
INSERT INTO `schedule_job_log` VALUES (98, 1, 'testTask', 'bootwaf', 0, NULL, 7, '2020-06-04 09:00:00');
INSERT INTO `schedule_job_log` VALUES (99, 1, 'testTask', 'bootwaf', 0, NULL, 38, '2020-06-04 09:30:00');
INSERT INTO `schedule_job_log` VALUES (100, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 10:00:00');
INSERT INTO `schedule_job_log` VALUES (101, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-04 10:30:00');
INSERT INTO `schedule_job_log` VALUES (102, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 11:00:00');
INSERT INTO `schedule_job_log` VALUES (103, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 11:30:00');
INSERT INTO `schedule_job_log` VALUES (104, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 12:00:00');
INSERT INTO `schedule_job_log` VALUES (105, 1, 'testTask', 'bootwaf', 0, NULL, 16, '2020-06-04 12:30:00');
INSERT INTO `schedule_job_log` VALUES (106, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-04 13:00:00');
INSERT INTO `schedule_job_log` VALUES (107, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-04 13:30:00');
INSERT INTO `schedule_job_log` VALUES (108, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 14:00:01');
INSERT INTO `schedule_job_log` VALUES (109, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-04 14:30:00');
INSERT INTO `schedule_job_log` VALUES (110, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-04 15:00:00');
INSERT INTO `schedule_job_log` VALUES (111, 1, 'testTask', 'bootwaf', 0, NULL, 15, '2020-06-04 15:30:00');
INSERT INTO `schedule_job_log` VALUES (112, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-04 16:00:00');
INSERT INTO `schedule_job_log` VALUES (113, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-05 10:30:00');
INSERT INTO `schedule_job_log` VALUES (114, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-05 11:30:00');
INSERT INTO `schedule_job_log` VALUES (115, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-05 12:00:00');
INSERT INTO `schedule_job_log` VALUES (116, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-05 12:30:00');
INSERT INTO `schedule_job_log` VALUES (117, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-05 13:00:00');
INSERT INTO `schedule_job_log` VALUES (118, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-05 13:30:00');
INSERT INTO `schedule_job_log` VALUES (119, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-05 14:00:00');
INSERT INTO `schedule_job_log` VALUES (120, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-05 14:30:00');
INSERT INTO `schedule_job_log` VALUES (121, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-05 15:00:00');
INSERT INTO `schedule_job_log` VALUES (122, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-05 15:30:00');
INSERT INTO `schedule_job_log` VALUES (123, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-05 16:00:00');
INSERT INTO `schedule_job_log` VALUES (124, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-05 16:30:00');
INSERT INTO `schedule_job_log` VALUES (125, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-05 17:00:00');
INSERT INTO `schedule_job_log` VALUES (126, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-05 17:30:00');
INSERT INTO `schedule_job_log` VALUES (127, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-08 09:00:00');
INSERT INTO `schedule_job_log` VALUES (128, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-08 09:30:00');
INSERT INTO `schedule_job_log` VALUES (129, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-08 10:00:00');
INSERT INTO `schedule_job_log` VALUES (130, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-08 10:30:00');
INSERT INTO `schedule_job_log` VALUES (131, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-08 11:30:00');
INSERT INTO `schedule_job_log` VALUES (132, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-08 12:00:00');
INSERT INTO `schedule_job_log` VALUES (133, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-08 12:30:00');
INSERT INTO `schedule_job_log` VALUES (134, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-08 13:00:00');
INSERT INTO `schedule_job_log` VALUES (135, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-08 13:30:00');
INSERT INTO `schedule_job_log` VALUES (136, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-08 14:00:00');
INSERT INTO `schedule_job_log` VALUES (137, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-08 14:30:00');
INSERT INTO `schedule_job_log` VALUES (138, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-08 15:00:00');
INSERT INTO `schedule_job_log` VALUES (139, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-08 15:30:00');
INSERT INTO `schedule_job_log` VALUES (140, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-08 16:00:00');
INSERT INTO `schedule_job_log` VALUES (141, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-08 17:00:00');
INSERT INTO `schedule_job_log` VALUES (142, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-08 17:30:00');
INSERT INTO `schedule_job_log` VALUES (143, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-09 09:30:00');
INSERT INTO `schedule_job_log` VALUES (144, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-09 10:00:00');
INSERT INTO `schedule_job_log` VALUES (145, 1, 'testTask', 'bootwaf', 0, NULL, 2, '2020-06-09 10:30:00');
INSERT INTO `schedule_job_log` VALUES (146, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-09 11:00:00');
INSERT INTO `schedule_job_log` VALUES (147, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-09 11:30:00');
INSERT INTO `schedule_job_log` VALUES (148, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-09 12:00:00');
INSERT INTO `schedule_job_log` VALUES (149, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-09 12:30:00');
INSERT INTO `schedule_job_log` VALUES (150, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-09 13:00:00');
INSERT INTO `schedule_job_log` VALUES (151, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-09 13:30:00');
INSERT INTO `schedule_job_log` VALUES (152, 1, 'testTask', 'bootwaf', 0, NULL, 0, '2020-06-09 14:00:00');
INSERT INTO `schedule_job_log` VALUES (153, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-09 14:30:00');
INSERT INTO `schedule_job_log` VALUES (154, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-09 15:30:00');
INSERT INTO `schedule_job_log` VALUES (155, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-09 16:00:00');
INSERT INTO `schedule_job_log` VALUES (156, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-09 17:00:00');
INSERT INTO `schedule_job_log` VALUES (157, 1, 'testTask', 'bootwaf', 0, NULL, 1, '2020-06-09 17:30:00');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `param_key`(`param_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'CLOUD_STORAGE_CONFIG_KEY', '{\"type\":1,\"qiniuDomain\":\"http://file.jr.casicloud.com\",\"qiniuPrefix\":\"b2b-test/upload\",\"qiniuAccessKey\":\"UVFsqUbvfPmloYwhmZDNiNCtgljg-nNabRiyL7ub\",\"qiniuSecretKey\":\"A_kDdc6QAUrEcrA5vg0umS6giByQdvhg_fXMaKmw\",\"qiniuBucketName\":\"muju-test\",\"aliyunDomain\":\"\",\"aliyunPrefix\":\"\",\"aliyunEndPoint\":\"\",\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qcloudBucketName\":\"\"}', 0, '云存储配置信息');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '上级部门ID，一级部门为0',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 0, 'bootwaf开源集团', 0, 0);
INSERT INTO `sys_dept` VALUES (2, 1, '长沙分公司', 1, 0);
INSERT INTO `sys_dept` VALUES (3, 1, '上海分公司', 2, 0);
INSERT INTO `sys_dept` VALUES (4, 3, '技术部', 0, 0);
INSERT INTO `sys_dept` VALUES (5, 3, '销售部', 1, 0);
INSERT INTO `sys_dept` VALUES (6, 1, '北京分公司', 1, -1);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典名称',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典码',
  `value` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典值',
  `order_num` int(11) NULL DEFAULT 0 COMMENT '排序',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除标记  -1：已删除  0：正常',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `type`(`type`, `code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数据字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, '性别', 'sex', '0', '女', 0, NULL, 0);
INSERT INTO `sys_dict` VALUES (2, '性别', 'sex', '1', '男', 1, NULL, 0);
INSERT INTO `sys_dict` VALUES (3, '性别', 'sex', '2', '未知', 3, NULL, 0);

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', NULL, NULL, 0, 'fa fa-cog', 0);
INSERT INTO `sys_menu` VALUES (2, 1, '管理员管理', 'modules/sys/user.html', NULL, 1, 'fa fa-user', 1);
INSERT INTO `sys_menu` VALUES (3, 1, '角色管理', 'modules/sys/role.html', NULL, 1, 'fa fa-user-secret', 2);
INSERT INTO `sys_menu` VALUES (4, 1, '菜单管理', 'modules/sys/menu.html', NULL, 1, 'fa fa-th-list', 3);
INSERT INTO `sys_menu` VALUES (5, 1, 'SQL监控', 'http://localhost:82/api/druid/sql.html', NULL, 1, 'fa fa-bug', 4);
INSERT INTO `sys_menu` VALUES (6, 1, '定时任务', 'modules/job/schedule.html', NULL, 1, 'fa fa-tasks', 5);
INSERT INTO `sys_menu` VALUES (7, 6, '查看', NULL, 'sys:schedule:list,sys:schedule:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (8, 6, '新增', NULL, 'sys:schedule:save', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (9, 6, '修改', NULL, 'sys:schedule:update', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (10, 6, '删除', NULL, 'sys:schedule:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (11, 6, '暂停', NULL, 'sys:schedule:pause', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (12, 6, '恢复', NULL, 'sys:schedule:resume', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (13, 6, '立即执行', NULL, 'sys:schedule:run', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (14, 6, '日志列表', NULL, 'sys:schedule:log', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (15, 2, '查看', NULL, 'sys:user:list,sys:user:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (16, 2, '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (17, 2, '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (18, 2, '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (19, 3, '查看', NULL, 'sys:role:list,sys:role:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (20, 3, '新增', NULL, 'sys:role:save,sys:menu:perms', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (21, 3, '修改', NULL, 'sys:role:update,sys:menu:perms', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (22, 3, '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (23, 4, '查看', NULL, 'sys:menu:list,sys:menu:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (24, 4, '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (25, 4, '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (26, 4, '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (27, 1, '参数管理', 'modules/sys/config.html', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'fa fa-sun-o', 6);
INSERT INTO `sys_menu` VALUES (29, 1, '系统日志', 'modules/sys/log.html', 'sys:log:list', 1, 'fa fa-file-text-o', 7);
INSERT INTO `sys_menu` VALUES (30, 1, '文件上传', 'modules/oss/oss.html', 'sys:oss:all', 1, 'fa fa-file-image-o', 6);
INSERT INTO `sys_menu` VALUES (31, 1, '部门管理', 'modules/sys/dept.html', NULL, 1, 'fa fa-file-code-o', 1);
INSERT INTO `sys_menu` VALUES (32, 31, '查看', NULL, 'sys:dept:list,sys:dept:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (33, 31, '新增', NULL, 'sys:dept:save,sys:dept:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (34, 31, '修改', NULL, 'sys:dept:update,sys:dept:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (35, 31, '删除', NULL, 'sys:dept:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (36, 1, '字典管理', 'modules/sys/dict.html', NULL, 1, 'fa fa-bookmark-o', 6);
INSERT INTO `sys_menu` VALUES (37, 36, '查看', NULL, 'sys:dict:list,sys:dict:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (38, 36, '新增', NULL, 'sys:dict:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (39, 36, '修改', NULL, 'sys:dict:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (40, 36, '删除', NULL, 'sys:dict:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (42, 1, '请求地址地图', 'modules/sys/path.html', NULL, 1, 'fa fa-file-code-o', 6);
INSERT INTO `sys_menu` VALUES (43, 42, '查看', NULL, 'sys:path:list,sys:path:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (44, 42, '新增', NULL, 'sys:path:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (45, 42, '修改', NULL, 'sys:path:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (46, 42, '删除', NULL, 'sys:path:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (47, 1, 'API接口', 'http://localhost:82/api/swagger-ui.html', NULL, 1, 'fa fa-file-code-o', 6);
INSERT INTO `sys_menu` VALUES (48, 1, '请求地址地图', 'modules/tb/test.html', NULL, 1, 'fa fa-file-code-o', 6);
INSERT INTO `sys_menu` VALUES (49, 48, '查看', NULL, 'tb:test:list,tb:test:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (50, 48, '新增', NULL, 'tb:test:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (51, 48, '修改', NULL, 'tb:test:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (52, 48, '删除', NULL, 'tb:test:delete', 2, NULL, 6);

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件上传' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oss
-- ----------------------------
INSERT INTO `sys_oss` VALUES (1, 'http://file.jr.casicloud.com/b2b-test/upload/20200608/8418e392d9d2441c865fd21820bf484a.png', '2020-06-08 16:20:26');

-- ----------------------------
-- Table structure for sys_path
-- ----------------------------
DROP TABLE IF EXISTS `sys_path`;
CREATE TABLE `sys_path`  (
  `id` bigint(25) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `clazz` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '所在类',
  `clazz_explain` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类说明',
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法名',
  `method_explain` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法说明',
  `request_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方式',
  `res_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '响应方式',
  `path_url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求路径',
  `for_users` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '限制用户',
  `for_roles` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '限制角色',
  `permissions` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所需权限',
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开发者',
  `create_by` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 73 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '请求地址地图' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_path
-- ----------------------------
INSERT INTO `sys_path` VALUES (1, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'run', '立即执行任务', '不限', '', 'sys/schedule/run', NULL, NULL, 'sys:schedule:run', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (2, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'update', '修改定时任务', '不限', '', 'sys/schedule/update', NULL, NULL, 'sys:schedule:update', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (3, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'delete', '删除定时任务', '不限', '', 'sys/schedule/delete', NULL, NULL, 'sys:schedule:delete', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (4, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'resume', '恢复定时任务', '不限', '', 'sys/schedule/resume', NULL, NULL, 'sys:schedule:resume', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (5, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'list', '定时任务列表', '不限', '', 'sys/schedule/list', NULL, NULL, 'sys:schedule:list', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (6, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'save', '保存定时任务', '不限', '', 'sys/schedule/save', NULL, NULL, 'sys:schedule:save', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (7, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'info', '定时任务信息', '不限', '', 'sys/schedule/info/{jobId}', NULL, NULL, 'sys:schedule:info', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (8, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'pause', '暂停定时任务', '不限', '', 'sys/schedule/pause', NULL, NULL, 'sys:schedule:pause', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (9, 'com.bwf.modules.job.controller.ScheduleJobLogController', '[定时任务日志]', 'list', '定时任务日志列表', '不限', '', 'sys/scheduleLog/list', NULL, NULL, 'sys:schedule:log', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (10, 'com.bwf.modules.job.controller.ScheduleJobLogController', '[定时任务日志]', 'info', '定时任务日志信息', '不限', '', 'sys/scheduleLog/info/{logId}', NULL, NULL, 'sys:schedule:log', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (11, 'com.bwf.modules.oss.controller.SysOssController', '[文件管理]', 'delete', ' 删除文件', '不限', '', 'sys/oss/delete', NULL, NULL, 'sys:oss:all', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (12, 'com.bwf.modules.oss.controller.SysOssController', '[文件管理]', 'list', '文件列表', '不限', '', 'sys/oss/list', NULL, NULL, 'sys:oss:all', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (13, 'com.bwf.modules.oss.controller.SysOssController', '[文件管理]', 'config', '云存储配置信息', '不限', '', 'sys/oss/config', NULL, NULL, 'sys:oss:all', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (14, 'com.bwf.modules.oss.controller.SysOssController', '[文件管理]', 'saveConfig', '保存云存储配置信息', '不限', '', 'sys/oss/saveConfig', NULL, NULL, 'sys:oss:all', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (15, 'com.bwf.modules.oss.controller.SysOssController', '[文件管理]', 'upload', '上传文件', '不限', '', 'sys/oss/upload', NULL, NULL, 'sys:oss:all', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (16, 'com.bwf.modules.sys.controller.SysConfigController', '[系统配置信息]', 'update', '修改配置', '不限', '', 'sys/config/update', NULL, NULL, 'sys:config:update', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (17, 'com.bwf.modules.sys.controller.SysConfigController', '[系统配置信息]', 'delete', '删除配置', '不限', '', 'sys/config/delete', NULL, NULL, 'sys:config:delete', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (18, 'com.bwf.modules.sys.controller.SysConfigController', '[系统配置信息]', 'list', '所有配置列表', '不限', '', 'sys/config/list', NULL, NULL, 'sys:config:list', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (19, 'com.bwf.modules.sys.controller.SysConfigController', '[系统配置信息]', 'save', '保存配置', '不限', '', 'sys/config/save', NULL, NULL, 'sys:config:save', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (20, 'com.bwf.modules.sys.controller.SysConfigController', '[系统配置信息]', 'info', '配置信息', '不限', '', 'sys/config/info/{id}', NULL, NULL, 'sys:config:info', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (21, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'update', '修改部门信息', '不限', '', 'sys/dept/update', NULL, NULL, 'sys:dept:update', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (22, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'delete', '删除部门信息', '不限', '', 'sys/dept/delete', NULL, NULL, 'sys:dept:delete', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (23, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'list', '部门列表', '不限', '', 'sys/dept/list', NULL, NULL, 'sys:dept:list', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (24, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'save', '保存部门信息', '不限', '', 'sys/dept/save', NULL, NULL, 'sys:dept:save', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (25, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'info', '信息', '不限', '', 'sys/dept/info/{deptId}', NULL, NULL, 'sys:dept:info', NULL, NULL, '2020-06-09 17:24:11', NULL, '2020-06-09 17:24:11', NULL, '0');
INSERT INTO `sys_path` VALUES (26, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'info', '上级部门Id(管理员则为0)', '不限', '', 'sys/dept/info', NULL, NULL, 'sys:dept:list', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (27, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'select', '选择部门(添加、修改菜单)', '不限', '', 'sys/dept/select', NULL, NULL, 'sys:dept:select', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (28, 'com.bwf.modules.sys.controller.SysDictController', '[数据字典]', 'update', '修改数据字典信息', '不限', '', 'sys/dict/update', NULL, NULL, 'sys:dict:update', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (29, 'com.bwf.modules.sys.controller.SysDictController', '[数据字典]', 'delete', '删除数据字典信息', '不限', '', 'sys/dict/delete', NULL, NULL, 'sys:dict:delete', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (30, 'com.bwf.modules.sys.controller.SysDictController', '[数据字典]', 'list', '数据字典列表', '不限', '', 'sys/dict/list', NULL, NULL, 'sys:dict:list', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (31, 'com.bwf.modules.sys.controller.SysDictController', '[数据字典]', 'save', '保存数据字典信息', '不限', '', 'sys/dict/save', NULL, NULL, 'sys:dict:save', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (32, 'com.bwf.modules.sys.controller.SysDictController', '[数据字典]', 'info', '数据字典信息', '不限', '', 'sys/dict/info/{id}', NULL, NULL, 'sys:dict:info', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (33, 'com.bwf.modules.sys.controller.SysLogController', '[系统日志]', 'list', '系统日志列表', '不限', '', 'sys/log/list', NULL, NULL, 'sys:log:list', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (34, 'com.bwf.modules.sys.controller.SysLoginController', '[登录相关]', 'logout', '退出', 'POST', '', '/logout', NULL, NULL, NULL, NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (35, 'com.bwf.modules.sys.controller.SysLoginController', '[登录相关]', 'captcha', '验证码', '不限', '', '/captcha.jpg', NULL, NULL, NULL, NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (36, 'com.bwf.modules.sys.controller.SysLoginController', '[登录相关]', 'login', '登录', '[POST]', '', '/login', NULL, NULL, NULL, NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (37, 'com.bwf.modules.sys.controller.SysLoginController', '[登录相关]', 'unlogin', '未登录', '不限', '', '/unlogin', NULL, NULL, NULL, NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (38, 'com.bwf.modules.sys.controller.SysLoginController', '[登录相关]', 'unauth', '无权限', '不限', '', '/unauth', NULL, NULL, NULL, NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (39, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'update', '修改菜单', '不限', '', 'sys/menu/update', NULL, NULL, 'sys:menu:update', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (40, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'delete', '删除菜单', '不限', '', 'sys/menu/delete', NULL, NULL, 'sys:menu:delete', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (41, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'list', '所有菜单列表', '不限', '', 'sys/menu/list', NULL, NULL, 'sys:menu:list', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (42, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'save', '保存菜单', '不限', '', 'sys/menu/save', NULL, NULL, 'sys:menu:save', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (43, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'info', '菜单信息', '不限', '', 'sys/menu/info/{menuId}', NULL, NULL, 'sys:menu:info', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (44, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'select', '选择菜单(添加、修改菜单)', '不限', '', 'sys/menu/select', NULL, NULL, 'sys:menu:select', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (45, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'nav', '导航菜单', '不限', '', 'sys/menu/nav', NULL, NULL, 'sys:menu:select', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (46, 'com.bwf.modules.sys.controller.SysPathController', '[所有后台请求地址]', 'update', '修改', '不限', '', 'sys/syspath/update', NULL, NULL, 'sys:path:update', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (47, 'com.bwf.modules.sys.controller.SysPathController', '[所有后台请求地址]', 'delete', '删除', '不限', '', 'sys/syspath/delete', NULL, NULL, 'sys:path:delete', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (48, 'com.bwf.modules.sys.controller.SysPathController', '[所有后台请求地址]', 'list', '列表', '不限', '', 'sys/syspath/list', NULL, NULL, 'sys:path:list', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (49, 'com.bwf.modules.sys.controller.SysPathController', '[所有后台请求地址]', 'save', '保存', '不限', '', 'sys/syspath/save', NULL, NULL, 'sys:path:save', NULL, NULL, '2020-06-09 17:24:12', NULL, '2020-06-09 17:24:12', NULL, '0');
INSERT INTO `sys_path` VALUES (50, 'com.bwf.modules.sys.controller.SysPathController', '[所有后台请求地址]', 'info', '信息', '不限', '', 'sys/syspath/info/{id}', NULL, NULL, 'sys:path:info', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (51, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'update', '修改角色', '不限', '', 'sys/role/update', NULL, NULL, 'sys:role:update', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (52, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'delete', '删除角色', '不限', '', 'sys/role/delete', NULL, NULL, 'sys:role:delete', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (53, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'list', '角色列表', '不限', '', 'sys/role/list', NULL, NULL, 'sys:role:list', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (54, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'save', '保存角色', '不限', '', 'sys/role/save', NULL, NULL, 'sys:role:save', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (55, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'info', '角色信息', '不限', '', 'sys/role/info/{roleId}', NULL, NULL, 'sys:role:info', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (56, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'select', '角色列表查询', '不限', '', 'sys/role/select', NULL, NULL, 'sys:role:select', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (57, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'update', '修改用户', '不限', '', 'sys/user/update', NULL, NULL, 'sys:user:update', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (58, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'delete', '删除用户', '不限', '', 'sys/user/delete', NULL, NULL, 'sys:user:delete', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (59, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'list', '所有用户列表', '不限', '', 'sys/user/list', NULL, NULL, 'sys:user:list', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (60, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'save', '保存用户', '不限', '', 'sys/user/save', NULL, NULL, 'sys:user:save', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (61, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'info', '获取登录的用户信息', '不限', '', 'sys/user/info', NULL, NULL, 'sys:user:save', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (62, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'info', '用户信息', '不限', '', 'sys/user/info/{userId}', NULL, NULL, 'sys:user:info', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (63, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'password', '修改登录用户密码', '不限', '', 'sys/user/password', NULL, NULL, 'sys:user:info', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (64, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'isPermitted', '判断当前登录用户的权限', '不限', '', 'sys/user/permission/isPermitted', NULL, NULL, 'sys:user:info', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (65, 'com.bwf.modules.tb.controller.ApiTestController', '[测试接口]', 'userInfo', '获取用户ID', 'GET', '', 'api/userId', NULL, NULL, NULL, NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (66, 'com.bwf.modules.tb.controller.ApiTestController', '[测试接口]', 'userInfo', '获取用户信息', 'GET', '', 'api/userInfo', NULL, NULL, NULL, NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (67, 'com.bwf.modules.tb.controller.ApiTestController', '[测试接口]', 'notToken', '忽略Token验证测试', 'GET', '', 'api/notToken', NULL, NULL, NULL, NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (68, 'com.bwf.modules.tb.controller.TestController', '[请求地址地图]', 'update', '请求地址地图修改', '不限', '', 'tb/test/update', NULL, NULL, 'tb:test:update', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (69, 'com.bwf.modules.tb.controller.TestController', '[请求地址地图]', 'delete', '请求地址地图删除', '不限', '', 'tb/test/delete', NULL, NULL, 'tb:test:delete', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (70, 'com.bwf.modules.tb.controller.TestController', '[请求地址地图]', 'list', '请求地址地图列表', '不限', '', 'tb/test/list', NULL, NULL, 'tb:test:list', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (71, 'com.bwf.modules.tb.controller.TestController', '[请求地址地图]', 'save', '请求地址地图保存', '不限', '', 'tb/test/save', NULL, NULL, 'tb:test:save', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');
INSERT INTO `sys_path` VALUES (72, 'com.bwf.modules.tb.controller.TestController', '[请求地址地图]', 'info', '请求地址地图信息', '不限', '', 'tb/test/info/{id}', NULL, NULL, 'tb:test:info', NULL, NULL, '2020-06-09 17:24:13', NULL, '2020-06-09 17:24:13', NULL, '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'testRole', NULL, 1, '2020-06-08 17:15:17');
INSERT INTO `sys_role` VALUES (2, 'tttt', '测试权限', 1, '2020-06-09 11:37:37');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与部门对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (6, 2, 1);
INSERT INTO `sys_role_dept` VALUES (7, 2, 2);
INSERT INTO `sys_role_dept` VALUES (8, 2, 4);
INSERT INTO `sys_role_dept` VALUES (9, 2, 5);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 145 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (78, 1, 1);
INSERT INTO `sys_role_menu` VALUES (79, 1, 2);
INSERT INTO `sys_role_menu` VALUES (80, 1, 15);
INSERT INTO `sys_role_menu` VALUES (81, 1, 17);
INSERT INTO `sys_role_menu` VALUES (82, 1, 18);
INSERT INTO `sys_role_menu` VALUES (83, 1, 3);
INSERT INTO `sys_role_menu` VALUES (84, 1, 19);
INSERT INTO `sys_role_menu` VALUES (85, 1, 20);
INSERT INTO `sys_role_menu` VALUES (86, 1, 21);
INSERT INTO `sys_role_menu` VALUES (87, 1, 22);
INSERT INTO `sys_role_menu` VALUES (88, 1, 4);
INSERT INTO `sys_role_menu` VALUES (89, 1, 23);
INSERT INTO `sys_role_menu` VALUES (90, 1, 24);
INSERT INTO `sys_role_menu` VALUES (91, 1, 25);
INSERT INTO `sys_role_menu` VALUES (92, 1, 26);
INSERT INTO `sys_role_menu` VALUES (93, 1, 5);
INSERT INTO `sys_role_menu` VALUES (94, 1, 6);
INSERT INTO `sys_role_menu` VALUES (95, 1, 7);
INSERT INTO `sys_role_menu` VALUES (96, 1, 8);
INSERT INTO `sys_role_menu` VALUES (97, 1, 9);
INSERT INTO `sys_role_menu` VALUES (98, 1, 10);
INSERT INTO `sys_role_menu` VALUES (99, 1, 11);
INSERT INTO `sys_role_menu` VALUES (100, 1, 12);
INSERT INTO `sys_role_menu` VALUES (101, 1, 13);
INSERT INTO `sys_role_menu` VALUES (102, 1, 14);
INSERT INTO `sys_role_menu` VALUES (103, 1, 27);
INSERT INTO `sys_role_menu` VALUES (104, 1, 29);
INSERT INTO `sys_role_menu` VALUES (105, 1, 30);
INSERT INTO `sys_role_menu` VALUES (106, 1, 31);
INSERT INTO `sys_role_menu` VALUES (107, 1, 32);
INSERT INTO `sys_role_menu` VALUES (108, 1, 33);
INSERT INTO `sys_role_menu` VALUES (109, 1, 34);
INSERT INTO `sys_role_menu` VALUES (110, 1, 35);
INSERT INTO `sys_role_menu` VALUES (111, 1, 36);
INSERT INTO `sys_role_menu` VALUES (112, 1, 37);
INSERT INTO `sys_role_menu` VALUES (113, 1, 38);
INSERT INTO `sys_role_menu` VALUES (114, 1, 39);
INSERT INTO `sys_role_menu` VALUES (115, 1, 40);
INSERT INTO `sys_role_menu` VALUES (116, 2, 1);
INSERT INTO `sys_role_menu` VALUES (117, 2, 2);
INSERT INTO `sys_role_menu` VALUES (118, 2, 15);
INSERT INTO `sys_role_menu` VALUES (119, 2, 16);
INSERT INTO `sys_role_menu` VALUES (120, 2, 3);
INSERT INTO `sys_role_menu` VALUES (121, 2, 19);
INSERT INTO `sys_role_menu` VALUES (122, 2, 21);
INSERT INTO `sys_role_menu` VALUES (123, 2, 4);
INSERT INTO `sys_role_menu` VALUES (124, 2, 23);
INSERT INTO `sys_role_menu` VALUES (125, 2, 26);
INSERT INTO `sys_role_menu` VALUES (126, 2, 6);
INSERT INTO `sys_role_menu` VALUES (127, 2, 7);
INSERT INTO `sys_role_menu` VALUES (128, 2, 9);
INSERT INTO `sys_role_menu` VALUES (129, 2, 11);
INSERT INTO `sys_role_menu` VALUES (130, 2, 13);
INSERT INTO `sys_role_menu` VALUES (131, 2, 14);
INSERT INTO `sys_role_menu` VALUES (132, 2, 31);
INSERT INTO `sys_role_menu` VALUES (133, 2, 32);
INSERT INTO `sys_role_menu` VALUES (134, 2, 33);
INSERT INTO `sys_role_menu` VALUES (135, 2, 34);
INSERT INTO `sys_role_menu` VALUES (136, 2, 35);
INSERT INTO `sys_role_menu` VALUES (137, 2, 36);
INSERT INTO `sys_role_menu` VALUES (138, 2, 37);
INSERT INTO `sys_role_menu` VALUES (139, 2, 38);
INSERT INTO `sys_role_menu` VALUES (140, 2, 40);
INSERT INTO `sys_role_menu` VALUES (141, 2, 42);
INSERT INTO `sys_role_menu` VALUES (142, 2, 43);
INSERT INTO `sys_role_menu` VALUES (143, 2, 45);
INSERT INTO `sys_role_menu` VALUES (144, 2, 46);

-- ----------------------------
-- Table structure for sys_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_token`;
CREATE TABLE `sys_token`  (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'token',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户Token' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '盐',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', 'e1153123d7d180ceeb820d577ff119876678732a68eef4e6ffc0b1f06a01f91b', 'YzcmCZNvbXocrsz9dm8e', 'root@bootwaf.io', '13612345678', 1, 1, '2016-11-11 11:11:11');
INSERT INTO `sys_user` VALUES (2, 'user', '1f12e52747a28e2d895f9a89792bff8f330461e95797a8c05d0ac21474ec1854', 'IVJLxym5wLsguhUVjAWF', '113@ww.com', '13222343341', 1, 4, '2020-06-05 16:47:35');
INSERT INTO `sys_user` VALUES (3, 'xudezhi', 'b635478420f4ddc0064bcf9886bf5e9498b78fecef444da7df9f74038a6f603e', 'H1IqFuHNxrsgZWv1k5ys', 'admin@ihuanzhi.com', NULL, 1, 4, '2020-06-08 13:50:49');
INSERT INTO `sys_user` VALUES (7, 'xudezhi1', 'c3eeb060958a2c2cbf5ac5df18c0cd37715132dc6c23c976d55d6e15cdddc236', 'zATtsZQrQtofzyWOUg6M', 'admin@ihuanzhi.com', NULL, 1, 5, '2020-06-08 15:42:12');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, 1);
INSERT INTO `sys_user_role` VALUES (2, 3, 1);
INSERT INTO `sys_user_role` VALUES (3, 7, 2);

-- ----------------------------
-- Table structure for tb_test
-- ----------------------------
DROP TABLE IF EXISTS `tb_test`;
CREATE TABLE `tb_test`  (
  `id` bigint(25) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `clazz` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '所在类',
  `clazz_explain` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类说明',
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法名',
  `method_explain` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法说明',
  `request_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方式',
  `res_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '响应方式',
  `path_url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求路径',
  `for_users` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '限制用户',
  `for_roles` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '限制角色',
  `permissions` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所需权限',
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开发者',
  `create_by` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_by` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '请求地址地图' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_test
-- ----------------------------
INSERT INTO `tb_test` VALUES (1, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'run', '立即执行任务', '不限', '', 'sys/schedule/run', NULL, NULL, 'sys:schedule:run', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (2, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'update', '修改定时任务', '不限', '', 'sys/schedule/update', NULL, NULL, 'sys:schedule:update', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (3, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'delete', '删除定时任务', '不限', '', 'sys/schedule/delete', NULL, NULL, 'sys:schedule:delete', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (4, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'resume', '恢复定时任务', '不限', '', 'sys/schedule/resume', NULL, NULL, 'sys:schedule:resume', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (5, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'list', '定时任务列表', '不限', '', 'sys/schedule/list', NULL, NULL, 'sys:schedule:list', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (6, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'save', '保存定时任务', '不限', '', 'sys/schedule/save', NULL, NULL, 'sys:schedule:save', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (7, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'info', '定时任务信息', '不限', '', 'sys/schedule/info/{jobId}', NULL, NULL, 'sys:schedule:info', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (8, 'com.bwf.modules.job.controller.ScheduleJobController', '[定时任务管理]', 'pause', '暂停定时任务', '不限', '', 'sys/schedule/pause', NULL, NULL, 'sys:schedule:pause', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (9, 'com.bwf.modules.job.controller.ScheduleJobLogController', '[定时任务日志]', 'list', '定时任务日志列表', '不限', '', 'sys/scheduleLog/list', NULL, NULL, 'sys:schedule:log', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (10, 'com.bwf.modules.job.controller.ScheduleJobLogController', '[定时任务日志]', 'info', '定时任务日志信息', '不限', '', 'sys/scheduleLog/info/{logId}', NULL, NULL, 'sys:schedule:log', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (11, 'com.bwf.modules.oss.controller.SysOssController', '[文件管理]', 'delete', ' 删除文件', '不限', '', 'sys/oss/delete', NULL, NULL, 'sys:oss:all', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (12, 'com.bwf.modules.oss.controller.SysOssController', '[文件管理]', 'list', '文件列表', '不限', '', 'sys/oss/list', NULL, NULL, 'sys:oss:all', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (13, 'com.bwf.modules.oss.controller.SysOssController', '[文件管理]', 'config', '云存储配置信息', '不限', '', 'sys/oss/config', NULL, NULL, 'sys:oss:all', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (14, 'com.bwf.modules.oss.controller.SysOssController', '[文件管理]', 'saveConfig', '保存云存储配置信息', '不限', '', 'sys/oss/saveConfig', NULL, NULL, 'sys:oss:all', NULL, NULL, '2020-06-09 16:30:03', NULL, '2020-06-09 16:30:03', NULL, '0');
INSERT INTO `tb_test` VALUES (15, 'com.bwf.modules.oss.controller.SysOssController', '[文件管理]', 'upload', '上传文件', '不限', '', 'sys/oss/upload', NULL, NULL, 'sys:oss:all', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (16, 'com.bwf.modules.sys.controller.SysConfigController', '[系统配置信息]', 'update', '修改配置', '不限', '', 'sys/config/update', NULL, NULL, 'sys:config:update', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (17, 'com.bwf.modules.sys.controller.SysConfigController', '[系统配置信息]', 'delete', '删除配置', '不限', '', 'sys/config/delete', NULL, NULL, 'sys:config:delete', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (18, 'com.bwf.modules.sys.controller.SysConfigController', '[系统配置信息]', 'list', '所有配置列表', '不限', '', 'sys/config/list', NULL, NULL, 'sys:config:list', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (19, 'com.bwf.modules.sys.controller.SysConfigController', '[系统配置信息]', 'save', '保存配置', '不限', '', 'sys/config/save', NULL, NULL, 'sys:config:save', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (20, 'com.bwf.modules.sys.controller.SysConfigController', '[系统配置信息]', 'info', '配置信息', '不限', '', 'sys/config/info/{id}', NULL, NULL, 'sys:config:info', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (21, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'update', '修改部门信息', '不限', '', 'sys/dept/update', NULL, NULL, 'sys:dept:update', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (22, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'delete', '删除部门信息', '不限', '', 'sys/dept/delete', NULL, NULL, 'sys:dept:delete', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (23, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'list', '部门列表', '不限', '', 'sys/dept/list', NULL, NULL, 'sys:dept:list', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (24, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'save', '保存部门信息', '不限', '', 'sys/dept/save', NULL, NULL, 'sys:dept:save', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (25, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'info', '信息', '不限', '', 'sys/dept/info/{deptId}', NULL, NULL, 'sys:dept:info', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (26, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'info', '上级部门Id(管理员则为0)', '不限', '', 'sys/dept/info', NULL, NULL, 'sys:dept:list', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (27, 'com.bwf.modules.sys.controller.SysDeptController', '[部门管理]', 'select', '选择部门(添加、修改菜单)', '不限', '', 'sys/dept/select', NULL, NULL, 'sys:dept:select', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (28, 'com.bwf.modules.sys.controller.SysDictController', '[数据字典]', 'update', '修改数据字典信息', '不限', '', 'sys/dict/update', NULL, NULL, 'sys:dict:update', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (29, 'com.bwf.modules.sys.controller.SysDictController', '[数据字典]', 'delete', '删除数据字典信息', '不限', '', 'sys/dict/delete', NULL, NULL, 'sys:dict:delete', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (30, 'com.bwf.modules.sys.controller.SysDictController', '[数据字典]', 'list', '数据字典列表', '不限', '', 'sys/dict/list', NULL, NULL, 'sys:dict:list', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (31, 'com.bwf.modules.sys.controller.SysDictController', '[数据字典]', 'save', '保存数据字典信息', '不限', '', 'sys/dict/save', NULL, NULL, 'sys:dict:save', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (32, 'com.bwf.modules.sys.controller.SysDictController', '[数据字典]', 'info', '数据字典信息', '不限', '', 'sys/dict/info/{id}', NULL, NULL, 'sys:dict:info', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (33, 'com.bwf.modules.sys.controller.SysLogController', '[系统日志]', 'list', '系统日志列表', '不限', '', 'sys/log/list', NULL, NULL, 'sys:log:list', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (34, 'com.bwf.modules.sys.controller.SysLoginController', '[登录相关]', 'logout', '退出', 'POST', '', '/logout', NULL, NULL, NULL, NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (35, 'com.bwf.modules.sys.controller.SysLoginController', '[登录相关]', 'captcha', '验证码', '不限', '', '/captcha.jpg', NULL, NULL, NULL, NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (36, 'com.bwf.modules.sys.controller.SysLoginController', '[登录相关]', 'login', '登录', '[POST]', '', '/login', NULL, NULL, NULL, NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (37, 'com.bwf.modules.sys.controller.SysLoginController', '[登录相关]', 'unlogin', '未登录', '不限', '', '/unlogin', NULL, NULL, NULL, NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (38, 'com.bwf.modules.sys.controller.SysLoginController', '[登录相关]', 'unauth', '无权限', '不限', '', '/unauth', NULL, NULL, NULL, NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (39, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'update', '修改菜单', '不限', '', 'sys/menu/update', NULL, NULL, 'sys:menu:update', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (40, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'delete', '删除菜单', '不限', '', 'sys/menu/delete', NULL, NULL, 'sys:menu:delete', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (41, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'list', '所有菜单列表', '不限', '', 'sys/menu/list', NULL, NULL, 'sys:menu:list', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (42, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'save', '保存菜单', '不限', '', 'sys/menu/save', NULL, NULL, 'sys:menu:save', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (43, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'info', '菜单信息', '不限', '', 'sys/menu/info/{menuId}', NULL, NULL, 'sys:menu:info', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (44, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'select', '选择菜单(添加、修改菜单)', '不限', '', 'sys/menu/select', NULL, NULL, 'sys:menu:select', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (45, 'com.bwf.modules.sys.controller.SysMenuController', '[系统菜单]', 'nav', '导航菜单', '不限', '', 'sys/menu/nav', NULL, NULL, 'sys:menu:select', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (46, 'com.bwf.modules.sys.controller.SysPathController', '[所有后台请求地址]', 'update', '修改', '不限', '', 'sys/syspath/update', NULL, NULL, 'sys:path:update', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (47, 'com.bwf.modules.sys.controller.SysPathController', '[所有后台请求地址]', 'delete', '删除', '不限', '', 'sys/syspath/delete', NULL, NULL, 'sys:path:delete', NULL, NULL, '2020-06-09 16:30:04', NULL, '2020-06-09 16:30:04', NULL, '0');
INSERT INTO `tb_test` VALUES (48, 'com.bwf.modules.sys.controller.SysPathController', '[所有后台请求地址]', 'list', '列表', '不限', '', 'sys/syspath/list', NULL, NULL, 'sys:path:list', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (49, 'com.bwf.modules.sys.controller.SysPathController', '[所有后台请求地址]', 'save', '保存', '不限', '', 'sys/syspath/save', NULL, NULL, 'sys:path:save', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (50, 'com.bwf.modules.sys.controller.SysPathController', '[所有后台请求地址]', 'info', '信息', '不限', '', 'sys/syspath/info/{id}', NULL, NULL, 'sys:path:info', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (51, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'update', '修改角色', '不限', '', 'sys/role/update', NULL, NULL, 'sys:role:update', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (52, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'delete', '删除角色', '不限', '', 'sys/role/delete', NULL, NULL, 'sys:role:delete', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (53, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'list', '角色列表', '不限', '', 'sys/role/list', NULL, NULL, 'sys:role:list', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (54, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'save', '保存角色', '不限', '', 'sys/role/save', NULL, NULL, 'sys:role:save', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (55, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'info', '角色信息', '不限', '', 'sys/role/info/{roleId}', NULL, NULL, 'sys:role:info', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (56, 'com.bwf.modules.sys.controller.SysRoleController', '[角色管理]', 'select', '角色列表查询', '不限', '', 'sys/role/select', NULL, NULL, 'sys:role:select', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (57, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'update', '修改用户', '不限', '', 'sys/user/update', NULL, NULL, 'sys:user:update', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (58, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'delete', '删除用户', '不限', '', 'sys/user/delete', NULL, NULL, 'sys:user:delete', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (59, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'list', '所有用户列表', '不限', '', 'sys/user/list', NULL, NULL, 'sys:user:list', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (60, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'save', '保存用户', '不限', '', 'sys/user/save', NULL, NULL, 'sys:user:save', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (61, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'info', '获取登录的用户信息', '不限', '', 'sys/user/info', NULL, NULL, 'sys:user:save', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (62, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'info', '用户信息', '不限', '', 'sys/user/info/{userId}', NULL, NULL, 'sys:user:info', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (63, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'password', '修改登录用户密码', '不限', '', 'sys/user/password', NULL, NULL, 'sys:user:info', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (64, 'com.bwf.modules.sys.controller.SysUserController', '[系统用户管理]', 'isPermitted', '判断当前登录用户的权限', '不限', '', 'sys/user/permission/isPermitted', NULL, NULL, 'sys:user:info', NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (65, 'com.bwf.modules.tb.controller.ApiTestController', '[测试接口]', 'userInfo', '获取用户ID', 'GET', '', 'api/userId', NULL, NULL, NULL, NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (66, 'com.bwf.modules.tb.controller.ApiTestController', '[测试接口]', 'userInfo', '获取用户信息', 'GET', '', 'api/userInfo', NULL, NULL, NULL, NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');
INSERT INTO `tb_test` VALUES (67, 'com.bwf.modules.tb.controller.ApiTestController', '[测试接口]', 'notToken', '忽略Token验证测试', 'GET', '', 'api/notToken', NULL, NULL, NULL, NULL, NULL, '2020-06-09 16:30:05', NULL, '2020-06-09 16:30:05', NULL, '0');

-- ----------------------------
-- Table structure for tb_token
-- ----------------------------
DROP TABLE IF EXISTS `tb_token`;
CREATE TABLE `tb_token`  (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'token',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户Token' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_token
-- ----------------------------
INSERT INTO `tb_token` VALUES (1, 'df55bfdef291487f821ad84ea22b6bf8', '2020-06-10 05:32:58', '2020-06-09 17:32:58');
INSERT INTO `tb_token` VALUES (3, '9b600c63856340bda4382e3c28b7fb3e', '2020-06-09 11:37:57', '2020-06-09 11:37:57');
INSERT INTO `tb_token` VALUES (7, '213b8263884444569defc024950f2e6b', '2020-06-09 23:38:06', '2020-06-09 11:38:06');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'mark', '13612345678', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '2017-03-23 22:37:41');

SET FOREIGN_KEY_CHECKS = 1;
