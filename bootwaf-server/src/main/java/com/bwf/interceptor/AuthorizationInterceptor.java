/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.interceptor;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.bwf.annotation.Login;
import com.bwf.common.exception.RRException;
import com.bwf.common.utils.R.RCode;
import com.bwf.modules.sys.entity.SysTokenEntity;
import com.bwf.modules.sys.entity.SysUserEntity;
import com.bwf.modules.sys.service.SysTokenService;
import com.bwf.modules.sys.shiro.ShiroUtils;

/**
 * 权限(Token)验证
 *
 * @author XDZ 1136277749@qq.com
 */
@Component
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {
	/**
	 * 日志处理类
	 */
	Logger logger=LoggerFactory.getLogger(getClass());
	
    @Autowired
    private SysTokenService tokenService;

    public static final String USER_KEY = "userId";

	@Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Login annotation;
        if(handler instanceof HandlerMethod) {
        	logger.info("包含Login注解");
            annotation = ((HandlerMethod) handler).getMethodAnnotation(Login.class);
        }else{
            return true;
        }

        if(annotation == null){
            return true;
        }
        
        /**
         * 后台判断登录信息是否还存在
         */
        if(!ShiroUtils.isLogin()) {
        	throw new RRException("登录失效，请重新登录",RCode.UN_LOGIN.getCode());
        }

        //从header中获取token
        String token = request.getHeader("token");
        //如果header中不存在token，则从参数中获取token
        if(StringUtils.isBlank(token)){
            token = request.getParameter("token");
        }

        //token为空
        if(StringUtils.isBlank(token)){
        	logger.info("token不能为空");
            throw new RRException("token不能为空",RCode.UN_LOGIN.getCode());
        }

        //查询token信息
        SysTokenEntity tokenEntity = tokenService.queryByToken(token);
        if(tokenEntity == null || tokenEntity.getExpireTime().getTime() < System.currentTimeMillis()){
        	
        	try {
        		//前台token失效则将后台用户信息退出（不用考虑token是否是当前用户的，都要重新登录）
        		SecurityUtils.getSubject().logout();
			} catch (Exception e) {
				//登出操纵，
				logger.info("因为没登录，所以调用登出失败！");
			}
        	logger.info("token失效，请重新登录");
			throw new RRException("token失效，请重新登录",RCode.UN_LOGIN.getCode());
        }

        /**
		 * 获取后台登录用户
		 */
		SysUserEntity sysUser=(SysUserEntity) SecurityUtils.getSubject().getPrincipal();
		/**
		 * 是相同用户，后台登录用户与前台token对应用户不一致
		 */
		if(!sysUser.getUserId().equals(tokenEntity.getUserId())) {
			logger.info("token信息校验不通过，请重新登录（token取到的用户和shiroSession取到的用户不一致）");
			throw new RRException("token信息校验不通过，请重新登录",401);
		}
        
        //设置userId到request里，后续根据userId，获取用户信息
        request.setAttribute(USER_KEY, tokenEntity.getUserId());

        return true;
    }
}
