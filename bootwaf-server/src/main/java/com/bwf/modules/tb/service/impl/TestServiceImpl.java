package com.bwf.modules.tb.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bwf.common.utils.PageUtils;
import com.bwf.common.utils.Query;

import com.bwf.modules.tb.dao.TestDao;
import com.bwf.modules.tb.entity.TestEntity;
import com.bwf.modules.tb.service.TestService;


/**
 * 请求地址地图
 *
 * @author xudezhi
 * @email 1136277749@qq.com
 * @date 2020-06-12 16:02:38
 */
@Service("testService")
public class TestServiceImpl extends ServiceImpl<TestDao, TestEntity> implements TestService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<TestEntity> page = this.page(
                new Query<TestEntity>().getPage(params),
                new QueryWrapper<TestEntity>()
        );

        return new PageUtils(page);
    }

}
