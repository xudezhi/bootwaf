package com.bwf.modules.tb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bwf.common.utils.PageUtils;
import com.bwf.modules.tb.entity.TestEntity;

import java.util.Map;

/**
 * 请求地址地图
 *
 * @author xudezhi
 * @email 1136277749@qq.com
 * @date 2020-06-12 16:02:38
 */
public interface TestService extends IService<TestEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

