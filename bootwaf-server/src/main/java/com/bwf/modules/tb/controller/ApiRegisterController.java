/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.tb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bwf.modules.sys.service.SysUserService;

import io.swagger.annotations.Api;

/**
 * 注册接口
 *
 * @author XDZ 1136277749@qq.com
 */
@RestController
@RequestMapping("api")
@Api(tags="注册接口")
public class ApiRegisterController {
    @Autowired
    private SysUserService userService;

//    @PostMapping("register")
//    @ApiOperation("注册")
//    public R register(@RequestBody RegisterForm form){
//        //表单校验
//        ValidatorUtils.validateEntity(form);
//
//        SysUserEntity user = new SysUserEntity();
//        user.setMobile(form.getMobile());
//        user.setUsername(form.getMobile());
//        user.setPassword(DigestUtils.sha256Hex(form.getPassword()));
//        user.setCreateTime(new Date());
//        userService.save(user);
//
//        return R.ok();
//    }
}
