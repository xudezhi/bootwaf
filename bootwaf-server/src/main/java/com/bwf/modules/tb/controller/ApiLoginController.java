/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.tb.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bwf.modules.sys.service.SysTokenService;
import com.bwf.modules.sys.service.SysUserService;

import io.swagger.annotations.Api;

/**
 * 登录接口
 *
 * @author XDZ 1136277749@qq.com
 */
@RestController
@RequestMapping("api")
@Api(tags="登录接口")
public class ApiLoginController {
    @Autowired
    private SysUserService userService;
    @Autowired
    private SysTokenService tokenService;


//    @PostMapping("login")
//    @ApiOperation("登录")
//    public R login(@RequestBody LoginForm form){
//        //表单校验
//        ValidatorUtils.validateEntity(form);
//
//        //用户登录
//        Map<String, Object> map = userService.login(form);
//
//        return R.ok(map);
//    }
    


}
