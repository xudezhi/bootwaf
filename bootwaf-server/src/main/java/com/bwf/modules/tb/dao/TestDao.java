package com.bwf.modules.tb.dao;

import com.bwf.modules.tb.entity.TestEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 请求地址地图
 * 
 * @author xudezhi
 * @email 1136277749@qq.com
 * @date 2020-06-12 16:02:38
 */
@Mapper
public interface TestDao extends BaseMapper<TestEntity> {
	
}
