/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.tb.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bwf.annotation.Login;
import com.bwf.annotation.LoginUser;
import com.bwf.common.utils.R;
import com.bwf.modules.sys.entity.SysUserEntity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 测试接口
 *
 * @author XDZ 1136277749@qq.com
 */
@RestController
@RequestMapping("api")
@Api(tags="测试接口")
public class ApiTestController {

    @Login
    @GetMapping(name="获取用户信息",value="userInfo")
    @ApiOperation(value="获取用户信息", response=SysUserEntity.class)
    public R userInfo(@ApiIgnore @LoginUser SysUserEntity user){
        return R.ok().put("user", user);
    }

    @Login
    @GetMapping("userId")
    @ApiOperation("获取用户ID")
    public R userInfo(@ApiIgnore @RequestAttribute("userId") Integer userId){
        return R.ok().put("userId", userId);
    }

    @GetMapping("notToken")
    @ApiOperation("忽略Token验证测试")
    public R notToken(){
        return R.ok().put("msg", "无需token也能访问。。。");
    }

}
