package com.bwf.modules.tb.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


import com.bwf.common.validator.group.AddGroup;
import com.bwf.common.validator.group.EditGroup;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * 请求地址地图
 * 
 * @author xudezhi
 * @email 1136277749@qq.com
 * @date 2020-06-12 16:02:38
 */
@ApiModel("请求地址地图信息")
@Data
@TableName("tb_test")
public class TestEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
	@ApiModelProperty("编号")
	@TableId
private Long id;
	/**
	 * 所在类
	 */
	@ApiModelProperty("所在类")
	@Length(min=0,max=65535,message="所在类 长度不超过65535 ",groups= {AddGroup.class,EditGroup.class})
private String clazz;
	/**
	 * 类说明
	 */
	@ApiModelProperty("类说明")
	@Length(min=0,max=255,message="类说明 长度不超过255 ",groups= {AddGroup.class,EditGroup.class})
private String clazzExplain;
	/**
	 * 方法名
	 */
	@ApiModelProperty("方法名")
	@Length(min=0,max=255,message="方法名 长度不超过255 ",groups= {AddGroup.class,EditGroup.class})
private String method;
	/**
	 * 方法说明
	 */
	@ApiModelProperty("方法说明")
	@Length(min=0,max=255,message="方法说明 长度不超过255 ",groups= {AddGroup.class,EditGroup.class})
private String methodExplain;
	/**
	 * 请求方式
	 */
	@ApiModelProperty("请求方式")
	@Length(min=0,max=255,message="请求方式 长度不超过255 ",groups= {AddGroup.class,EditGroup.class})
private String requestMethod;
	/**
	 * 响应方式
	 */
	@ApiModelProperty("响应方式")
	@Length(min=0,max=64,message="响应方式 长度不超过64 ",groups= {AddGroup.class,EditGroup.class})
private String resType;
	/**
	 * 请求路径
	 */
	@ApiModelProperty("请求路径")
	@Length(min=0,max=65535,message="请求路径 长度不超过65535 ",groups= {AddGroup.class,EditGroup.class})
private String pathUrl;
	/**
	 * 限制用户
	 */
	@ApiModelProperty("限制用户")
	@Length(min=0,max=255,message="限制用户 长度不超过255 ",groups= {AddGroup.class,EditGroup.class})
private String forUsers;
	/**
	 * 限制角色
	 */
	@ApiModelProperty("限制角色")
	@Length(min=0,max=255,message="限制角色 长度不超过255 ",groups= {AddGroup.class,EditGroup.class})
private String forRoles;
	/**
	 * 所需权限
	 */
	@ApiModelProperty("所需权限")
	@Length(min=0,max=255,message="所需权限 长度不超过255 ",groups= {AddGroup.class,EditGroup.class})
private String permissions;
	/**
	 * 开发者
	 */
	@ApiModelProperty("开发者")
	@Length(min=0,max=255,message="开发者 长度不超过255 ",groups= {AddGroup.class,EditGroup.class})
private String author;
	/**
	 * 创建者
	 */
	@ApiModelProperty("创建者")
private Long createBy;
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	@NotNull(message="创建时间不能为空",groups= {AddGroup.class,EditGroup.class})
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
private Date createDate;
	/**
	 * 更新者
	 */
	@ApiModelProperty("更新者")
private Long updateBy;
	/**
	 * 更新时间
	 */
	@ApiModelProperty("更新时间")
	@NotNull(message="更新时间不能为空",groups= {AddGroup.class,EditGroup.class})
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
private Date updateDate;
	/**
	 * 备注信息
	 */
	@ApiModelProperty("备注信息")
	@Length(min=0,max=255,message="备注信息 长度不超过255 ",groups= {AddGroup.class,EditGroup.class})
private String remarks;
	/**
	 * 删除标记
	 */
	@ApiModelProperty("删除标记")
	@NotNull(message="删除标记不能为空",groups= {AddGroup.class,EditGroup.class})
	@Length(min=1,max=1,message="删除标记 长度为1到 1 ",groups= {AddGroup.class,EditGroup.class})
private String delFlag;

}
