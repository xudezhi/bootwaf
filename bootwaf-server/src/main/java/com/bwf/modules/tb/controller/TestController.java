package com.bwf.modules.tb.controller;

import java.util.Arrays;
import java.util.Map;

import com.bwf.common.validator.ValidatorUtils;
import com.bwf.common.validator.group.AddGroup;
import com.bwf.common.validator.group.EditGroup;
import com.bwf.modules.sys.controller.AbstractController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bwf.modules.tb.entity.TestEntity;
import com.bwf.modules.tb.service.TestService;
import com.bwf.common.utils.PageUtils;
import com.bwf.common.utils.R;

import com.bwf.annotation.Login;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 请求地址地图
 *
 * @author xudezhi
 * @email 1136277749@qq.com
 * @date 2020-06-12 16:02:38
 */
@Api(tags="请求地址地图")
@RestController
@RequestMapping("tb/test")
public class TestController extends AbstractController{
    @Autowired
    private TestService testService;

    /**
     * 列表
     */
    @Login
   	@ApiOperation("请求地址地图列表")
    @RequestMapping("/list")
    @RequiresPermissions("tb:test:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = testService.queryPage(params);
		
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @Login
   	@ApiOperation("请求地址地图信息")
    @RequestMapping("/info/{id}")
    @RequiresPermissions("tb:test:info")
    public R info(@PathVariable("id") Long id){
        TestEntity test = testService.getById(id);

        return R.ok().put("test", test);
    }

    /**
     * 保存
     */
    @Login
   	@ApiOperation("请求地址地图保存")
    @RequestMapping("/save")
    @RequiresPermissions("tb:test:save")
    public R save(@RequestBody TestEntity test){
    	/**
    	 *验证数据
    	 */
    	ValidatorUtils.validateEntity(test,AddGroup.class);
        testService.save(test);

        return R.ok();
    }

    /**
     * 修改
     */
    @Login
   	@ApiOperation("请求地址地图修改")
    @RequestMapping("/update")
    @RequiresPermissions("tb:test:update")
    public R update(@RequestBody TestEntity test){
    	/**
    	 *验证数据
    	 */
        ValidatorUtils.validateEntity(test,EditGroup.class);
        testService.updateById(test);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @Login
   	@ApiOperation("请求地址地图删除")
    @RequestMapping("/delete")
    @RequiresPermissions("tb:test:delete")
    public R delete(@RequestBody Long[] ids){
        testService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
