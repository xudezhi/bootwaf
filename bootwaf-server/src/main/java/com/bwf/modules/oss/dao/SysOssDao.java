/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.oss.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bwf.modules.oss.entity.SysOssEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件上传
 *
 * @author XDZ 1136277749@qq.com
 */
@Mapper
public interface SysOssDao extends BaseMapper<SysOssEntity> {
	
}
