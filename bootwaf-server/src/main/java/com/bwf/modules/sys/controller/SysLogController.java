/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.sys.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bwf.annotation.Login;
import com.bwf.common.utils.PageUtils;
import com.bwf.common.utils.R;
import com.bwf.modules.sys.service.SysLogService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * 系统日志
 *
 * @author XDZ 1136277749@qq.com
 */
@Api(tags="系统日志")
@RestController
@RequestMapping("sys/log")
public class SysLogController  extends AbstractController{
	@Autowired
	private SysLogService sysLogService;
	
	/**
	 * 列表
	 */
    @Login
   	@ApiOperation("系统日志列表")
	@RequestMapping("list")
	@RequiresPermissions("sys:log:list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = sysLogService.queryPage(params);

		return R.ok().put("page", page);
	}
	
}
