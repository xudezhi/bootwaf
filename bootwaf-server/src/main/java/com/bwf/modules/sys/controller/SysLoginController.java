/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.sys.controller;


import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bwf.annotation.Login;
import com.bwf.annotation.LoginUser;
import com.bwf.common.utils.R;
import com.bwf.common.utils.R.RCode;
import com.bwf.modules.sys.entity.SysTokenEntity;
import com.bwf.modules.sys.entity.SysUserEntity;
import com.bwf.modules.sys.service.SysTokenService;
import com.bwf.modules.sys.shiro.ShiroUtils;
import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 登录相关
 *
 * @author XDZ 1136277749@qq.com
 */
@Api(tags="登录相关")
@RestController
public class SysLoginController {
	@Autowired
	private Producer producer;
	
    @Autowired
    private SysTokenService sysTokenService;

    @RequestMapping("captcha.jpg")
	@ApiOperation("验证码")
	public void captcha(HttpServletResponse response)throws IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //生成文字验证码
        String text = producer.createText();
        //生成图片验证码
        BufferedImage image = producer.createImage(text);
        //保存到shiro session
        ShiroUtils.setSessionAttribute(Constants.KAPTCHA_SESSION_KEY, text);
        
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
	}
	
	/**
	 * 登录
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ApiOperation("登录")
	public R login(String username, String password, String captcha) {
		String kaptcha = ShiroUtils.getKaptcha(Constants.KAPTCHA_SESSION_KEY);
		if(!captcha.equalsIgnoreCase(kaptcha)){
			return R.error("验证码不正确");
		}
		
		try{
			Subject subject = ShiroUtils.getSubject();
			UsernamePasswordToken token = new UsernamePasswordToken(username, password);
			subject.login(token);
		}catch (UnknownAccountException e) {
			return R.error(e.getMessage());
		}catch (IncorrectCredentialsException e) {
			return R.error("账号或密码不正确");
		}catch (LockedAccountException e) {
			return R.error("账号已被锁定,请联系管理员");
		}catch (AuthenticationException e) {
			return R.error("账户验证失败");
		}
		/**
		 * 获取当前登录用户
		 */
		SysUserEntity user=(SysUserEntity) SecurityUtils.getSubject().getPrincipal();
		/**
		 * 创建ticket
		 */
		SysTokenEntity sysToken=sysTokenService.createToken(user.getUserId());
	    
		return R.ok().put("token",sysToken.getToken()).put("expire_time", sysToken.getExpireTime());
	}
	
	/**
	 * 退出
	 */
	@Login
    @PostMapping("logout")
    @ApiOperation("退出")
    public R logout(@LoginUser SysUserEntity user){
		
		/**
		 * 将ticket失效
		 */
		sysTokenService.expireToken(user.getUserId());
		/**
		 * 从shiro移除登录
		 */
		ShiroUtils.logout();
		
		return R.error(RCode.UN_LOGIN, "未登录");
	}
	
	
    @RequestMapping("unlogin")
    @ApiOperation("未登录")
    public R unlogin(){
        return R.error(RCode.UN_LOGIN, "未登录");
    }
    
    @RequestMapping("unauth")
    @ApiOperation("无权限")
    public R unauth(){
    	 return R.error(RCode.NO_POWER, "无权限");
    }
    
}
