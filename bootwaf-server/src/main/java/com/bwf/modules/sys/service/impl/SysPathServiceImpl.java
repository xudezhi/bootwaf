package com.bwf.modules.sys.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bwf.common.utils.PageUtils;
import com.bwf.common.utils.Query;
import com.bwf.modules.sys.dao.SysPathDao;
import com.bwf.modules.sys.entity.SysPathEntity;
import com.bwf.modules.sys.service.SysPathService;


@Service("sysPathService")
public class SysPathServiceImpl extends ServiceImpl<SysPathDao, SysPathEntity> implements SysPathService {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SysPathEntity> page = this.page(
                new Query<SysPathEntity>().getPage(params),
                new QueryWrapper<SysPathEntity>().like(params.get("methodExplain")!=null, "method_explain", params.get("methodExplain"))
        );

        return new PageUtils(page);
    }

}
