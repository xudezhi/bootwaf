package com.bwf.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bwf.annotation.Login;
import com.bwf.common.utils.PageUtils;
import com.bwf.common.utils.R;
import com.bwf.common.validator.ValidatorUtils;
import com.bwf.modules.sys.entity.SysPathEntity;
import com.bwf.modules.sys.service.SysPathService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;



/**
 * 请求地址地图
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2020-06-04 16:10:47
 */
@Api(tags="所有后台请求地址")
@RestController
@RequestMapping("sys/syspath")
public class SysPathController  extends AbstractController{
    @Autowired
    private SysPathService sysPathService;

    /**
     * 列表
     */
	@Login
   	@ApiOperation("列表")
    @RequestMapping("list")
    @RequiresPermissions("sys:path:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysPathService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
	@Login
   	@ApiOperation("信息")
    @RequestMapping("info/{id}")
    @RequiresPermissions("sys:path:info")
    public R info(@PathVariable("id") String id){
        SysPathEntity sysPath = sysPathService.getById(id);

        return R.ok().put("sysPath", sysPath);
    }

    /**
     * 保存
     */
	@Login
   	@ApiOperation("保存")
    @RequestMapping("save")
    @RequiresPermissions("sys:path:save")
    public R save(@RequestBody SysPathEntity sysPath){
        sysPathService.save(sysPath);

        return R.ok();
    }

    /**
     * 修改
     */
	@Login
   	@ApiOperation("修改")
    @RequestMapping("update")
    @RequiresPermissions("sys:path:update")
    public R update(@RequestBody SysPathEntity sysPath){
        ValidatorUtils.validateEntity(sysPath);
        sysPathService.updateById(sysPath);
        
        return R.ok();
    }

    /**
     * 删除
     */
	@Login
   	@ApiOperation("删除")
    @RequestMapping("delete")
    @RequiresPermissions("sys:path:delete")
    public R delete(@RequestBody String[] ids){
        sysPathService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
