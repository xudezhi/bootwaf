/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 部门管理
 *
 * @author XDZ 1136277749@qq.com
 */
@ApiModel("部门信息")
@Data
@TableName("sys_dept")
public class SysDeptEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 部门ID
	 */
	@ApiModelProperty("部门ID")
	@TableId
	private Long deptId;
	/**
	 * 上级部门ID，一级部门为0
	 */
	@ApiModelProperty("上级部门ID，一级部门为0")
	private Long parentId;
	/**
	 * 部门名称
	 */
	@ApiModelProperty("部门名称")
	private String name;
	/**
	 * 上级部门名称
	 */
	@ApiModelProperty("上级部门名称")
	@TableField(exist=false)
	private String parentName;
	
	@ApiModelProperty("部门编号")
	private Integer orderNum;
	
	@ApiModelProperty("删除状态")
	@TableLogic
	private Integer delFlag;
	/**
	 * ztree属性
	 */
	@ApiModelProperty("ztree属性")
	@TableField(exist=false)
	private Boolean open;
	
	@ApiModelProperty("ztree集合")
	@TableField(exist=false)
	private List<?> list;
}
