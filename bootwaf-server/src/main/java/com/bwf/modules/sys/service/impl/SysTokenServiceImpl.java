/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.sys.service.impl;

import java.util.Date;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bwf.modules.sys.dao.SysTokenDao;
import com.bwf.modules.sys.entity.SysTokenEntity;
import com.bwf.modules.sys.service.SysTokenService;


@Service("sysTokenService")
public class SysTokenServiceImpl extends ServiceImpl<SysTokenDao, SysTokenEntity> implements SysTokenService {
	/**
	 * 12小时后过期
	 */
	private final static int EXPIRE = 3600 * 12;

	@Override
	public SysTokenEntity queryByToken(String token) {
		return this.getOne(new QueryWrapper<SysTokenEntity>().eq("token", token));
	}

	@Override
	public SysTokenEntity createToken(long userId) {
		//当前时间
		Date now = new Date();
		//过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);

		//生成token
		String token = generateToken();

		//保存或更新用户token
		SysTokenEntity tokenEntity = new SysTokenEntity();
		tokenEntity.setUserId(userId);
		tokenEntity.setToken(token);
		tokenEntity.setUpdateTime(now);
		tokenEntity.setExpireTime(expireTime);
		this.saveOrUpdate(tokenEntity);

		return tokenEntity;
	}

	@Override
	public void expireToken(long userId){
		Date now = new Date();

		SysTokenEntity tokenEntity = new SysTokenEntity();
		tokenEntity.setUserId(userId);
		tokenEntity.setUpdateTime(now);
		tokenEntity.setExpireTime(now);
		this.saveOrUpdate(tokenEntity);
	}

	private String generateToken(){
		return UUID.randomUUID().toString().replace("-", "");
	}
}
