/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bwf.modules.sys.entity.SysTokenEntity;

/**
 * 用户Token
 *
 * @author XDZ 1136277749@qq.com
 */
public interface SysTokenService extends IService<SysTokenEntity> {

	SysTokenEntity queryByToken(String token);

	/**
	 * 生成token
	 * @param userId  用户ID
	 * @return        返回token信息
	 */
	SysTokenEntity createToken(long userId);

	/**
	 * 设置token过期
	 * @param userId 用户ID
	 */
	void expireToken(long userId);

}
