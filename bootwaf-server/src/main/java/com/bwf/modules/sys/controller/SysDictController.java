/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bwf.annotation.Login;
import com.bwf.common.utils.PageUtils;
import com.bwf.common.utils.R;
import com.bwf.common.validator.ValidatorUtils;
import com.bwf.modules.sys.entity.SysDictEntity;
import com.bwf.modules.sys.service.SysDictService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 数据字典
 *
 * @author XDZ 1136277749@qq.com
 */
@Api(tags="数据字典")
@RestController
@RequestMapping("sys/dict")
public class SysDictController  extends AbstractController{
    @Autowired
    private SysDictService sysDictService;

    /**
     * 列表
     */
    @Login
	@ApiOperation("数据字典列表")
    @RequestMapping("list")
    @RequiresPermissions("sys:dict:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysDictService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @Login
   	@ApiOperation("数据字典信息")
    @RequestMapping("info/{id}")
    @RequiresPermissions("sys:dict:info")
    public R info(@PathVariable("id") Long id){
        SysDictEntity dict = sysDictService.getById(id);

        return R.ok().put("dict", dict);
    }

    /**
     * 保存
     */
    @Login
   	@ApiOperation("保存数据字典信息")
    @RequestMapping("save")
    @RequiresPermissions("sys:dict:save")
    public R save(@RequestBody SysDictEntity dict){
        //校验类型
        ValidatorUtils.validateEntity(dict);

        sysDictService.save(dict);

        return R.ok();
    }

    /**
     * 修改
     */
    @Login
   	@ApiOperation("修改数据字典信息")
    @RequestMapping("update")
    @RequiresPermissions("sys:dict:update")
    public R update(@RequestBody SysDictEntity dict){
        //校验类型
        ValidatorUtils.validateEntity(dict);

        sysDictService.updateById(dict);

        return R.ok();
    }

    /**
     * 删除
     */
    @Login
   	@ApiOperation("删除数据字典信息")
    @RequestMapping("delete")
    @RequiresPermissions("sys:dict:delete")
    public R delete(@RequestBody Long[] ids){
        sysDictService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
