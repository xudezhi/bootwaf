package com.bwf.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 请求地址地图
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2020-06-04 16:10:47
 */
@ApiModel("接口地址信息")
@Data
@TableName("sys_path")
public class SysPathEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
	@ApiModelProperty("编号")
	@TableId
	private Long id;
	/**
	 * 所在类
	 */
	@ApiModelProperty("所在类")
	private String clazz;
	/**
	 * 类说明
	 */
	@ApiModelProperty("类说明")
	private String clazzExplain;
	/**
	 * 方法名
	 */
	@ApiModelProperty("方法名")
	private String method;
	/**
	 * 方法说明
	 */
	@ApiModelProperty("方法说明")
	private String methodExplain;
	/**
	 * 请求方式
	 */
	@ApiModelProperty("请求方式")
	private String requestMethod;
	/**
	 * 响应方式
	 */
	@ApiModelProperty("响应方式")
	private String resType;
	/**
	 * 请求路径
	 */
	@ApiModelProperty("请求路径")
	private String pathUrl;
	/**
	 * 限制用户
	 */
	@ApiModelProperty("限制用户")
	private String forUsers;
	/**
	 * 限制角色
	 */
	@ApiModelProperty("限制角色")
	private String forRoles;
	/**
	 * 所需权限
	 */
	@ApiModelProperty("所需权限")
	private String permissions;
	/**
	 * 开发者
	 */
	@ApiModelProperty("开发者")
	private String author;
	/**
	 * 创建者
	 */
	@ApiModelProperty("创建者")
	private Long createBy;
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date createDate;
	/**
	 * 更新者
	 */
	@ApiModelProperty("更新者")
	private Long updateBy;
	/**
	 * 更新时间
	 */
	@ApiModelProperty(" 更新时间")
	private Date updateDate;
	/**
	 * 备注信息
	 */
	@ApiModelProperty("备注信息")
	private String remarks;
	/**
	 * 删除标记
	 */
	@ApiModelProperty("删除标记")
	private String delFlag;

}
