/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;



/**
 * 用户Token
 *
 * @author XDZ 1136277749@qq.com
 */
@ApiModel("用户token")
@Data
@TableName("tb_token")
public class SysTokenEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	@ApiModelProperty("角色ID")
	@TableId(type= IdType.INPUT)
	private Long userId;
	
	@ApiModelProperty("token")
	private String token;
	/**
	 * 过期时间
	 */
	@ApiModelProperty("过期时间")
	private Date expireTime;
	/**
	 * 更新时间
	 */
	@ApiModelProperty("更新时间")
	private Date updateTime;

}
