/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.sys.controller;


import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bwf.annotation.Login;
import com.bwf.common.annotation.SysLog;
import com.bwf.common.exception.RRException;
import com.bwf.common.utils.PageUtils;
import com.bwf.common.utils.R;
import com.bwf.common.validator.Assert;
import com.bwf.common.validator.ValidatorUtils;
import com.bwf.common.validator.group.AddGroup;
import com.bwf.common.validator.group.UpdateGroup;
import com.bwf.modules.sys.entity.SysUserEntity;
import com.bwf.modules.sys.service.SysUserRoleService;
import com.bwf.modules.sys.service.SysUserService;
import com.bwf.modules.sys.shiro.ShiroUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 系统用户
 *
 * @author XDZ 1136277749@qq.com
 */
@Api(tags="系统用户管理")
@RestController
@RequestMapping("sys/user")
public class SysUserController extends AbstractController {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserRoleService sysUserRoleService;
	
	/**
	 * 判断当前登录用户的权限
	 * @return
	 */
	@Login
	@ApiOperation("判断当前登录用户的权限")
	@RequestMapping("permission/isPermitted")
	public R isPermitted(@ApiParam("指定权限") String permission) {
		try {
			boolean isPermitted=SecurityUtils.getSubject().isPermitted(permission);
			logger.info("用户是否拥有："+permission+"的权限："+isPermitted);
			
			return R.ok().put("isPermitted", isPermitted);
		} catch (Exception e) {
			throw new RRException("校验权限异常");
		}
	}
	
	/**
	 * 所有用户列表
	 */
	@Login
	@ApiOperation("所有用户列表")
	@RequestMapping("list")
	@RequiresPermissions("sys:user:list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = sysUserService.queryPage(params);

		return R.ok().put("page", page);
	}
	
	/**
	 * 获取登录的用户信息
	 */
	@Login
	@ApiOperation("获取登录的用户信息")
	@RequestMapping("info")
	public R info(){
		return R.ok().put("user", getUser());
	}
	
	/**
	 * 修改登录用户密码
	 */
	@Login
	@ApiOperation("修改登录用户密码")
	@SysLog("修改密码")
	@RequestMapping("password")
	public R password(String password, String newPassword){
		Assert.isBlank(newPassword, "新密码不为能空");

		//原密码
		password = ShiroUtils.sha256(password, getUser().getSalt());
		//新密码
		newPassword = ShiroUtils.sha256(newPassword, getUser().getSalt());
				
		//更新密码
		boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword);
		if(!flag){
			return R.error("原密码不正确");
		}
		
		return R.ok();
	}
	
	/**
	 * 用户信息
	 */
	@Login
	@ApiOperation("用户信息")
	@RequestMapping("info/{userId}")
	@RequiresPermissions("sys:user:info")
	public R info(@PathVariable("userId") Long userId){
		SysUserEntity user = sysUserService.getById(userId);
		
		//获取用户所属的角色列表
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		user.setRoleIdList(roleIdList);
		
		return R.ok().put("user", user);
	}
	
	/**
	 * 保存用户
	 */
	@Login
	@ApiOperation("保存用户")
	@SysLog("保存用户")
	@RequestMapping("save")
	@RequiresPermissions("sys:user:save")
	public R save(@RequestBody SysUserEntity user){
		ValidatorUtils.validateEntity(user, AddGroup.class);
		
		sysUserService.saveUser(user);
		
		return R.ok();
	}
	
	/**
	 * 修改用户
	 */
	@Login
	@ApiOperation("修改用户")
	@SysLog("修改用户")
	@RequestMapping("update")
	@RequiresPermissions("sys:user:update")
	public R update(@RequestBody SysUserEntity user){
		ValidatorUtils.validateEntity(user, UpdateGroup.class);

		sysUserService.update(user);
		
		return R.ok();
	}
	
	/**
	 * 删除用户
	 */
	@Login
	@ApiOperation("删除用户")
	@SysLog("删除用户")
	@RequestMapping("delete")
	@RequiresPermissions("sys:user:delete")
	public R delete(@RequestBody Long[] userIds){
		if(ArrayUtils.contains(userIds, 1L)){
			return R.error("系统管理员不能删除");
		}
		
		if(ArrayUtils.contains(userIds, getUserId())){
			return R.error("当前用户不能删除");
		}

		sysUserService.removeByIds(Arrays.asList(userIds));
		
		return R.ok();
	}
}
