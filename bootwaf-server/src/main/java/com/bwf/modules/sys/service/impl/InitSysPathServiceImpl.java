package com.bwf.modules.sys.service.impl;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bwf.common.utils.scan.ScanPathUtils;
import com.bwf.modules.sys.dao.SysPathDao;
import com.bwf.modules.sys.entity.SysPathEntity;


@Service
public class InitSysPathServiceImpl extends ServiceImpl<SysPathDao, SysPathEntity>{
	protected Logger logger = LoggerFactory.getLogger(getClass());
	

    /**
	 * 项目启动时，初始化定时器
	 */
	@PostConstruct
	public void init(){
		SysPathDao sysPathDao=baseMapper;
		sysPathDao.truncate();
		try {
			/**
			 * 创建类加载去
			 */
			ClassLoader classLoader = getClass().getClassLoader();
			/**
			 * 配置加载更庐江
			 */
			URL url = classLoader.getResource("com/bwf");
			/**
			 * 编码转换处理中文和空格
			 */
			String rootPath = URLDecoder.decode(url.getPath(),"utf-8").toString();
			/**
			 * 读取路径
			 */
			File rootFile = new File(rootPath);
			/**
			 * 获取路径下文件及文件夹集合
			 */
			File[] files = rootFile.listFiles();
			/**
			 * 获取根实际路径，用于之后的对比截取
			 */
			String absolutePath=rootFile.getAbsolutePath();
			/**
			 * 调用文件循环进行扫描读取
			 */
			ScanPathUtils.fileIterator(sysPathDao,files,absolutePath);
		} catch (Exception e) {
			logger.info("初始化扫描请求路径信息失败！");
			logger.info("异常信息："+e.getMessage());
		}
		
	}
    
}
