package com.bwf.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bwf.common.utils.PageUtils;
import com.bwf.modules.sys.entity.SysPathEntity;

import java.util.Map;

/**
 * 请求地址地图
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2020-06-04 16:10:47
 */
public interface SysPathService extends IService<SysPathEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

