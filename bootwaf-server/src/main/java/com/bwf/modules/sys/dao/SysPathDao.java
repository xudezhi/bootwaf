package com.bwf.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bwf.modules.sys.entity.SysPathEntity;

/**
 * IP地址
 *
 * @author XDZ 1136277749@qq.com
 */
@Mapper
public interface SysPathDao extends BaseMapper<SysPathEntity> {
	
	public void truncate();
}
