/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 系统配置信息
 *
 * @author XDZ 1136277749@qq.com
 */
@Data
@TableName("sys_config")
@ApiModel(value = "系统配置")
public class SysConfigEntity {
	
	@TableId
	@ApiModelProperty("id")
	private Long id;
	
	@ApiModelProperty("参数名")
	@NotBlank(message="参数名不能为空")
	private String paramKey;
	
	@ApiModelProperty("参数值")
	@NotBlank(message="参数值不能为空")
	private String paramValue;
	
	@ApiModelProperty("备注")
	private String remark;

}
