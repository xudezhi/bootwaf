/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bwf.modules.sys.entity.SysTokenEntity;

/**
 * 用户Token
 *
 * @author XDZ 1136277749@qq.com
 */
@Mapper
public interface SysTokenDao extends BaseMapper<SysTokenEntity> {
	
}
