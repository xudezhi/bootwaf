/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.modules.job.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 定时任务日志
 *
 * @author XDZ 1136277749@qq.com
 */
@ApiModel("定时任务日志")
@Data
@TableName("schedule_job_log")
public class ScheduleJobLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 日志id
	 */
	@ApiModelProperty("日志id")
	@TableId
	private Long logId;
	
	/**
	 * 任务id
	 */
	@ApiModelProperty("任务id")
	private Long jobId;
	
	/**
	 * spring bean名称
	 */
	@ApiModelProperty("spring bean名称")
	private String beanName;
	
	/**
	 * 参数
	 */
	@ApiModelProperty("参数")
	private String params;
	
	/**
	 * 任务状态    0：成功    1：失败
	 */
	@ApiModelProperty("任务状态    0：成功    1：失败")
	private Integer status;
	
	/**
	 * 失败信息
	 */
	@ApiModelProperty("失败信息")
	private String error;
	
	/**
	 * 耗时(单位：毫秒)
	 */
	@ApiModelProperty("耗时(单位：毫秒)")
	private Integer times;
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	
}
