/**
 * Copyright (c) 2016-2019 bootwaf开源 All rights reserved.
 *
 * http://www.ihuanzhi.com
 *
 * 版权所有，侵权必究！
 */

package com.bwf.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * WebMvc配置
 *
 * @author XDZ 1136277749@qq.com
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/statics/**").addResourceLocations("classpath:/statics/");
    }

}