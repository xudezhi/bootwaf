package com.bwf.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 类的控制说明接口
 * @author xudz
 * 20181108
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
public @interface ApiExplain {
	/**
	 * 注解值,即控制器和方法的说明内容
	 * @return
	 */
	String value() default "未注释";
	/**
	 * 作者
	 * @return
	 */
	String author() default "徐德志";
	
}
